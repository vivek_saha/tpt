using DG.Tweening;
using UnityEngine;

public class State_Side_Show : MonoBehaviour
{
	[HideInInspector]
	public PlayerElements playerElements;

	private void Awake()
	{
		playerElements = GetComponent<PlayerElements>();
	}

	public void SideShowSender()
	{
		if (playerElements.isActive)
		{
			playerElements.isTimerRunning = false;
			playerElements.firstTimer = false;
			playerElements.secondTimer = false;
			playerElements.ResetPlayerTimer();
			playerElements.ResetColor();
			if (playerElements.isLocalPlayer)
			{
				playerElements.tElements.NotMyTurn();
			}
		}
	}

	public void SideShowReceiver()
	{
		playerElements.isTimerRunning = false;
		playerElements.secondTimer = false;
		playerElements.firstTimer = false;
		playerElements.ResetPlayerTimer();
		playerElements.ResetColor();
		if (playerElements.isLocalPlayer)
		{
			playerElements.tElements.NotMyTurn();
			playerElements.tElements.OpenSideShowPanel();
		}
	}

	public void SideShowAccept()
	{
		playerElements.acceptSideShowGO.SetActive(value: true);
		playerElements.acceptSideShowGO.transform.localScale = Vector3.zero;
		playerElements.acceptSideShowGO.transform.DOScale(Vector3.one, 1f).SetEase(Ease.OutBack).OnComplete(delegate
		{
			playerElements.acceptSideShowGO.transform.DOScale(Vector3.zero, 0.5f).SetDelay(3f);
		});
		playerElements.declineSideShowGO.SetActive(value: false);
		if (playerElements.isActive && playerElements.isLocalPlayer)
		{
			playerElements.tElements.CloseSideShowPanel();
		}
	}

	public void SideShowDecline()
	{
		playerElements.acceptSideShowGO.SetActive(value: false);
		playerElements.declineSideShowGO.SetActive(value: true);
		playerElements.declineSideShowGO.transform.localScale = Vector3.zero;
		playerElements.declineSideShowGO.transform.DOScale(Vector3.one, 1f).SetEase(Ease.OutBack).OnComplete(delegate
		{
			playerElements.declineSideShowGO.transform.DOScale(Vector3.zero, 0.5f).SetDelay(3f);
		});
		if (playerElements.isActive && playerElements.isLocalPlayer)
		{
			playerElements.tElements.CloseSideShowPanel();
		}
	}
}
