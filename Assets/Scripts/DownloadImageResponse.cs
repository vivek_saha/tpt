using System;
using UnityEngine;

[Serializable]
public class DownloadImageResponse
{
	public string image_key;

	public string image_name;

	public string image_url;

	public static DownloadImageResponse CreateFromJSON(string data)
	{
		return JsonUtility.FromJson<DownloadImageResponse>(data);
	}
}
