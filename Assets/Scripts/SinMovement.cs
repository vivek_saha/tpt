using UnityEngine;

public class SinMovement : MonoBehaviour
{
	private Vector3 _newPosition;

	private void Start()
	{
		_newPosition = base.transform.position;
	}

	private void Update()
	{
		_newPosition = base.transform.position;
		_newPosition.y += Mathf.Sin(Time.time) * Time.deltaTime;
		base.transform.position = _newPosition;
	}
}
