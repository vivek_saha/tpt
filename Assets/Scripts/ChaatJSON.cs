using System;

[Serializable]
public class ChaatJSON
{
	public string playerId;

	public string messages;

	public ChaatJSON(string _playerId, string _messages)
	{
		playerId = _playerId;
		messages = _messages;
	}
}
