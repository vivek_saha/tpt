using TMPro;
using UnityEngine;

public class LocalizedText : MonoBehaviour
{
	public string key;

	private void Start()
	{
		TextMeshProUGUI component = GetComponent<TextMeshProUGUI>();
		if (LocalizationManager.instance.isHindi)
		{
			component.font = LocalizationManager.instance._hiFont;
		}
		else
		{
			component.font = LocalizationManager.instance._enFont;
		}
		component.text = LocalizationManager.instance.GetLocalizedValue(key);
	}
}
