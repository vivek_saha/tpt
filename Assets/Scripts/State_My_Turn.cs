using UnityEngine;

public class State_My_Turn : MonoBehaviour
{
	[HideInInspector]
	public PlayerElements playerElements;

	private void Awake()
	{
		playerElements = GetComponent<PlayerElements>();
	}

	public void StartTimer()
	{
		playerElements.firstTimer = true;
		playerElements.secondTimer = true;
		playerElements.timerSlider.value = 0f;
		playerElements.ResetColor();
		playerElements.isTimerRunning = true;
	}

	public void MyTurn()
	{
		if (playerElements.isLocalPlayer)
		{
			playerElements.tElements.MyTurn();
		}
	}
}
