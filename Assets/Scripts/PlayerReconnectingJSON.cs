using System;

[Serializable]
public class PlayerReconnectingJSON
{
	public string playerId;

	public PlayerReconnectingJSON(string _playerId)
	{
		playerId = _playerId;
	}
}
