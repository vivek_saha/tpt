using UnityEngine;
using UnityEngine.UI;

public class ToggleSounds : MonoBehaviour
{
	[SerializeField]
	private Sprite soundOn;

	[SerializeField]
	private Sprite soundOff;

	[SerializeField]
	private Image soundButtonImage;

	private void OnEnable()
	{
		if (PlayerPrefs.GetInt("Sound") == 0)
		{
			soundButtonImage.sprite = soundOff;
		}
		else
		{
			soundButtonImage.sprite = soundOn;
		}
	}

	public void ToggleSound()
	{
		UnityEngine.Debug.Log("Sound Testing..." + PlayerPrefs.GetInt("Sound"));
		if (PlayerPrefs.GetInt("Sound") == 0)
		{
			PlayerPrefs.SetInt("Sound", 1);
			soundButtonImage.sprite = soundOn;
			AudioManager.instance.PlaySound("click");
		}
		else
		{
			PlayerPrefs.SetInt("Sound", 0);
			soundButtonImage.sprite = soundOff;
		}
	}
}
