using EasyMobile;
using Facebook.Unity;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UniRx;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class FillMainMenu : MonoBehaviour
{
	[Header("Profile Elements")]
	public Text profileNameText;

	[SerializeField]
	private TextMeshProUGUI profileChipsText;

	[SerializeField]
	private TextMeshProUGUI profileForceSideshowText;

	public Image profileImage;

	[SerializeField]
	private TextMeshProUGUI bonusTimerText;

	[SerializeField]
	private Button bonusTimerButton;

	[SerializeField]
	private Transform menuParent;

	[SerializeField]
	private GameObject menuGO;

	public UpdateProfileData profileData;

	public Image initialProfileImage;

	[SerializeField]
	private Button nameChangeButton;

	public GameObject vipGO;

	public Sprite profileSprite;

	[Header("Bonus Elements")]
	[SerializeField]
	private TextMeshProUGUI rewardText;

	[SerializeField]
	private TextMeshProUGUI forceSideShowText;

	[Header("Deals Elements")]
	[SerializeField]
	private GameObject dealsPanelGO;

	[SerializeField]
	private TextMeshProUGUI dealsChipsText;

	[SerializeField]
	private BuyDeals buyDeals;

	[Header("Package In-App Elements")]
	[SerializeField]
	private GameObject inAppGO;

	[SerializeField]
	private GameObject inAppPrefab;

	[Header("BootAmount")]
	[SerializeField]
	private PlayNowBootAmountList playNowBootAmount;

	[SerializeField]
	private NoLimitBootAmountList noLimitBootAmountList;

	[SerializeField]
	private PrivateBootAmountList privateBootAmountList;

	[SerializeField]
	private TournamentBootAmountList tournamentBootAmountList;

	[SerializeField]
	private MuflisBootAmountList variationLimitBootAmountList;

	[SerializeField]
	private MuflisNoLimitBootAmountList variationNoLimitBootAmountList;

	[SerializeField]
	private OpenClosePopup exitPopup;

	[SerializeField]
	private OpenClosePopup watchAdsPopup;

	[SerializeField]
	private OpenClosePopup promotionalPopup;

	[SerializeField]
	private Image promotionalImage;

	[SerializeField]
	private string promotionalURLString;

	public TMP_InputField privateTableIF;

	public long privateTableBootAmount;

	private float bonusTimer;

	private bool startBonusTimer;

	[Header("Video Seen Reward")]
	public OpenClosePopup videoSeenGO;

	public TextMeshProUGUI videoChipsReward;

	public TextMeshProUGUI videoForceReward;

	public TapToOpenBonus tapToOpenBonus;

	[Header("Booted Out Canvas")]
	public OpenClosePopup bootedOutPopup;

	public TextMeshProUGUI bootedOutTitle;

	public TextMeshProUGUI bootedOutBody;

	public OpenClosePopup vipDetailsGO;

	public OpenClosePopup bonusOpenGO;

	private void Awake()
	{
		TableListing();
		/*if (GameManager.Instance.promotionalImageDisplay == 1)
		{
			DisplayPromotionalImage();
		}*/
		GameManager.Instance.sCanvas.settingsNameText.text = URL.Instance.TrimName(GameManager.Instance.localPlayerName);
		GameManager.Instance.sCanvas.settingsIDText.text = "ID : " + GameManager.Instance.id;
		profileChipsText.text = GameManager.Instance.format.FormatNumberOnTable(GameManager.Instance.localPlayerChips);
		profileForceSideshowText.text = GameManager.Instance.format.FormatNumberOnTable(GameManager.Instance.localPlayerForceSideshow);
		if (GameManager.Instance.isVIP)
		{
			vipGO.SetActive(true);
		}
		else
		{
			vipGO.SetActive(false);
		}
		profileNameText.text = URL.Instance.TrimNameForOthers(GameManager.Instance.localPlayerName, GameManager.Instance.isFacebookLogin, GameManager.Instance.tempName);
		if (GameManager.Instance.isFacebookLogin)
		{
			nameChangeButton.gameObject.SetActive(value: true);
			profileData.proText.text = URL.Instance.TrimName(GameManager.Instance.localPlayerName);
			if (GameManager.Instance.tempName == "-1")
			{
				profileData.nameToggles[0].isOn = true;
			}
			else
			{
				for (int i = 0; i < profileData.nameToggles.Length; i++)
				{
					if (profileData.nameToggles[i].GetComponentInChildren<TextMeshProUGUI>().text == GameManager.Instance.tempName)
					{
						profileData.nameToggles[i].isOn = true;
					}
				}
			}
			FB.API("/me/picture?type=square&height=128&width=128", HttpMethod.GET, delegate(IGraphResult result)
			{
				if (result.Error == null)
				{
					GameManager.Instance.sCanvas.settingsProfileImage.sprite = Sprite.Create(result.Texture, new Rect(0f, 0f, result.Texture.width, result.Texture.height), default(Vector2));
					initialProfileImage.sprite = Sprite.Create(result.Texture, new Rect(0f, 0f, result.Texture.width, result.Texture.height), default(Vector2));
					if (GameManager.Instance.tempImage == "-1")
					{
						profileImage.sprite = Sprite.Create(result.Texture, new Rect(0f, 0f, result.Texture.width, result.Texture.height), default(Vector2));
						profileData.profileToggles[0].isOn = true;
					}
					else
					{
						profileData.profileToggles[int.Parse(GameManager.Instance.tempImage) + 1].isOn = true;
						GameManager.Instance.LoadImagesCO(profileImage, GameManager.Instance.tempImage);
					}
				}
			});
		}
		else
		{
			nameChangeButton.gameObject.SetActive(false);
			profileNameText.text = URL.Instance.TrimName(GameManager.Instance.localPlayerName);
			GameManager.Instance.sCanvas.settingsProfileImage.sprite = profileSprite;
		}
		if (GameManager.Instance.lastbonusTime != 14400f)
		{
			bonusTimer = GameManager.Instance.lastbonusTime;
			startBonusTimer = true;
		}
		else
		{
			startBonusTimer = false;
		}
		GameManager.Instance.LogMessage("Main Menu");
		//HeyzapAds.Start("73b58dda351d4116bd18d13400cbfef0", 1);
		if (GameManager.Instance.localPlayerChips <= 500)
		{
			GameManager.Instance.inAppValuesCanvas.PackageListing();
		}
	}

	private void Start()
	{
		//RewardedVideoCallback();
		//RequestRewardedVideo();
	}

	/*private void DisplayPromotionalImage()
	{
		promotionalPopup.ClosePopup();
		ObservableWWW.Get(URL.Instance.CALLAPI(URL.Instance.PROMOTIONAL_IMAGE, string.Empty)).Subscribe(delegate(string x)
		{
			URL.Instance.DebugPrint("DisplayPromotionalImage " + GameManager.Instance.security.Decrypt(x));
			PlayerData playerData = PlayerData.CreateFromJSON(GameManager.Instance.security.Decrypt(x));
			if (playerData.status == "success")
			{
				promotionalURLString = playerData.response.promotionalURLString;
				StartCoroutine(DownloadPromotionalImage(playerData.response.imageUrl));
			}
		}, delegate(Exception ex)
		{
			Debug.LogException(ex);
			NativeUI.ShowToast("Oh Snap! Something went wrong :(", isLongToast: true);
			GameManager.Instance.loadingCanvas.LoadingState(_isActive: false, _tip: false);
		});
	}

	public IEnumerator DownloadPromotionalImage(string _url)
	{
		WWW www = new WWW(_url);
		yield return www;
		Sprite tex = Sprite.Create(www.texture, new Rect(0f, 0f, www.texture.width, www.texture.height), Vector2.zero);
		promotionalImage.sprite = tex;
		promotionalPopup.OpenPopup();
		GameManager.Instance.promotionalImageDisplay++;
	}*/

	public void ClickPromotionalImage()
	{
		if (!(promotionalURLString == string.Empty) && promotionalURLString.Length > 0)
		{
			Application.OpenURL(promotionalURLString);
		}
	}

	public void PackageListing()
	{
		GameManager.Instance.inAppValuesCanvas.PackageListing();
	}

	public void VideoReward()
	{
		GameManager.Instance.loadingCanvas.LoadingState(_isActive: true, _tip: false);
		JSONObject jSONObject = new JSONObject();
		jSONObject.AddField("playerId", GameManager.Instance.localPlayerID);
		ObservableWWW.Get(URL.Instance.CALLAPI(URL.Instance.VIDEO_REWARD, jSONObject.ToString())).Subscribe(delegate(string x)
		{
			URL.Instance.DebugPrint("VideoRewardCO " + GameManager.Instance.security.Decrypt(x));
			PlayerData playerData = PlayerData.CreateFromJSON(GameManager.Instance.security.Decrypt(x));
			if (playerData.status == "error")
			{
				videoSeenGO.ClosePopup();
			}
			else if (playerData.status == "success")
			{
				UpdateProfileSectionWithAdBonus(playerData.response.reward, playerData.response.forcesideshow);
				GameManager.Instance.LogMessage("Video Seen Completed");
			}
			GameManager.Instance.loadingCanvas.LoadingState(_isActive: false, _tip: false);
		}, delegate(Exception ex)
		{
			Debug.LogException(ex);
			NativeUI.ShowToast("Oh Snap! Something went wrong :(", isLongToast: true);
			GameManager.Instance.loadingCanvas.LoadingState(_isActive: false, _tip: false);
		});
	}

	public void DailyBonus()
	{
		AudioManager.instance.PlaySound("click");
		AudioManager.instance.StopSound("bonus-idle");
		AudioManager.instance.PlaySound("bonus-open");
		JSONObject jSONObject = new JSONObject();
		jSONObject.AddField("playerId", GameManager.Instance.localPlayerID);

		Debug.Log(jSONObject.ToString());

		ObservableWWW.Get(URL.Instance.CALLAPI(URL.Instance.DAILY_REWARD_API, jSONObject.ToString())).Subscribe(delegate(string x)
		{
			URL.Instance.DebugPrint("Daily Reward: " + GameManager.Instance.security.Decrypt(x));
			PlayerData playerData = PlayerData.CreateFromJSON(GameManager.Instance.security.Decrypt(x));
			if (playerData.status == "error")
			{
				bonusTimer = float.Parse(playerData.response.timediff);
				TimeSpan timeSpan = TimeSpan.FromSeconds(bonusTimer);
				string text = $"{timeSpan.Hours:D2}h:{timeSpan.Minutes:D2}m:{timeSpan.Seconds:D2}s";
				bonusTimerText.text = text;
				NativeUI.ShowToast("Wait for " + text);
				AudioManager.instance.StopSound("bonus-open");
				AudioManager.instance.StopSound("bonus-idle");
				AudioManager.instance.StopSound("bonus-impact");
				if (bonusTimer > 0f)
				{
					startBonusTimer = true;
				}
				else
				{
					startBonusTimer = false;
				}
				AudioManager.instance.StopSound("bonus-open");
			}
			else if (playerData.status == "success")
			{
				GameManager.Instance.LogMessage("Bonus Claimed");
				GameManager.Instance.lastbonusTime = 14399f;
				bonusTimerButton.interactable = false;
				UpdateProfileSection(playerData.response.reward, playerData.response.forcesideshow);
				startBonusTimer = true;
				bonusTimer = 14400f;
				tapToOpenBonus.chipsText.text = GameManager.Instance.format.FormatNumberOnTable(long.Parse(playerData.response.reward));
				tapToOpenBonus.forceText.text = GameManager.Instance.format.FormatNumberOnTable(long.Parse(playerData.response.forcesideshow));
				SetLocalNotification();
			}
		}, delegate(Exception ex)
		{
			Debug.LogException(ex);
			GameManager.Instance.LogMessage("Bonus Claimed Error : " + ex);
			NativeUI.ShowToast("Oh Snap! Something went wrong :(", isLongToast: true);
			AudioManager.instance.StopSound("bonus-open");
		});
	}

	private void SetLocalNotification()
	{
		string notificationID = GameManager.Instance.notificationID;
		Dictionary<string, object> dictionary = new Dictionary<string, object>();
		dictionary["contents"] = new Dictionary<string, string>
		{
			{
				"en",
				"Your bonus is ready. Click here to claim it."
			}
		};
		dictionary["include_player_ids"] = new List<string>
		{
			notificationID
		};
		dictionary["send_after"] = DateTime.Now.ToUniversalTime().AddSeconds(14400.0).ToString("U");
		/*OneSignal.PostNotification(dictionary, delegate
		{
			NativeUI.ShowToast("Oh Great! We will notify you once your bonus is ready.");
		}, delegate(Dictionary<string, object> responseFailure)
		{
			NativeUI.ShowToast(Json.Serialize(responseFailure));
		});*/
	}

	private void UpdateProfileSection(string _reward, string _forceSideShow)
	{
		GameManager.Instance.localPlayerChips += long.Parse(_reward);
		GameManager.Instance.localPlayerForceSideshow += long.Parse(_forceSideShow);
		rewardText.text = GameManager.Instance.format.FormatNumberOnTable(long.Parse(_reward));
		forceSideShowText.text = GameManager.Instance.format.FormatNumberOnTable(long.Parse(_forceSideShow));
		profileChipsText.text = GameManager.Instance.format.FormatNumberOnTable(GameManager.Instance.localPlayerChips);
		profileForceSideshowText.text = GameManager.Instance.format.FormatNumberOnTable(GameManager.Instance.localPlayerForceSideshow);
		bonusOpenGO.OpenPopup();
	}

	private void UpdateProfileSectionWithAdBonus(string _reward, string _forceSideShow)
	{
		GameManager.Instance.localPlayerChips += long.Parse(_reward);
		GameManager.Instance.localPlayerForceSideshow += long.Parse(_forceSideShow);
		rewardText.text = GameManager.Instance.format.FormatNumberOnTable(long.Parse(_reward));
		forceSideShowText.text = GameManager.Instance.format.FormatNumberOnTable(long.Parse(_forceSideShow));
		profileChipsText.text = GameManager.Instance.format.FormatNumberOnTable(GameManager.Instance.localPlayerChips);
		profileForceSideshowText.text = GameManager.Instance.format.FormatNumberOnTable(GameManager.Instance.localPlayerForceSideshow);
		videoChipsReward.text = GameManager.Instance.format.FormatNumberOnTable(long.Parse(_reward));
		videoForceReward.text = GameManager.Instance.format.FormatNumberOnTable(long.Parse(_forceSideShow));
		videoSeenGO.OpenPopup();
	}

	public void OpenDealsMenu()
	{
		GameManager.Instance.inAppValuesCanvas.DealsListing();
	}

	public void TableListing()
	{
		JSONObject jSONObject = new JSONObject();
		jSONObject.AddField("playerId", GameManager.Instance.localPlayerID);
		ObservableWWW.Get(URL.Instance.CALLAPI(URL.Instance.TABLE_LIST, jSONObject.ToString())).Subscribe(delegate(string x)
		{
			URL.Instance.DebugPrint(GameManager.Instance.security.Decrypt(x));
			TableData tableData = TableData.CreateFromJSON(GameManager.Instance.security.Decrypt(x));
			if (tableData.status == "success")
			{
				for (int i = 0; i < tableData.response.Length; i++)
				{
					if (tableData.response[i].type == "limit")
					{
						for (int j = 0; j < tableData.response[i].bootAmount.Length; j++)
						{
							playNowBootAmount.playNowBootAmounts[j].SetActive(value: true);
							playNowBootAmount.playNowBootAmounts[j].GetComponent<GetBootAmount>().bootAmount = tableData.response[i].bootAmount[j];
							playNowBootAmount.playNowBootAmounts[j].GetComponent<GetBootAmount>().bootAmountText.text = GameManager.Instance.format.FormatNumber(long.Parse(tableData.response[i].bootAmount[j]));
						}
					}
					else if (tableData.response[i].type == "nolimit")
					{
						for (int k = 0; k < tableData.response[i].bootAmount.Length; k++)
						{
							noLimitBootAmountList.noLimitBootAmounts[k].SetActive(value: true);
							noLimitBootAmountList.noLimitBootAmounts[k].GetComponent<GetBootAmount>().bootAmount = tableData.response[i].bootAmount[k];
							noLimitBootAmountList.noLimitBootAmounts[k].GetComponent<GetBootAmount>().bootAmountText.text = GameManager.Instance.format.FormatNumber(long.Parse(tableData.response[i].bootAmount[k]));
						}
					}
					else if (tableData.response[i].type == "private")
					{
						for (int l = 0; l < tableData.response[i].bootAmount.Length; l++)
						{
							URL.Instance.DebugPrint("Private Table : " + tableData.response[i].bootAmount[l]);
							privateBootAmountList.privateBootAmounts[l].SetActive(value: true);
							privateBootAmountList.privateBootAmounts[l].GetComponent<GetBootAmount>().bootAmount = tableData.response[i].bootAmount[l];
							privateBootAmountList.privateBootAmounts[l].GetComponent<GetBootAmount>().bootAmountText.text = GameManager.Instance.format.FormatNumber(long.Parse(tableData.response[i].bootAmount[l]));
						}
					}
					else if (tableData.response[i].type == "OLC")
					{
						for (int m = 0; m < tableData.response[i].bootAmount.Length; m++)
						{
							URL.Instance.DebugPrint("OLC : " + tableData.response[i].bootAmount[m]);
							tournamentBootAmountList.tournamentBootAmount[m].SetActive(value: true);
							tournamentBootAmountList.tournamentBootAmount[m].GetComponent<GetBootAmount>().bootAmount = tableData.response[i].bootAmount[m];
							tournamentBootAmountList.tournamentBootAmount[m].GetComponent<GetBootAmount>().bootAmountText.text = GameManager.Instance.format.FormatNumber(long.Parse(tableData.response[i].bootAmount[m]));
						}
					}
					else if (tableData.response[i].type == "variation_limit")
					{
						for (int n = 0; n < tableData.response[i].bootAmount.Length; n++)
						{
							URL.Instance.DebugPrint("variation_limit : " + tableData.response[i].bootAmount[n]);
							variationLimitBootAmountList.muflisBootAmount[n].SetActive(value: true);
							variationLimitBootAmountList.muflisBootAmount[n].GetComponent<GetBootAmount>().bootAmount = tableData.response[i].bootAmount[n];
							variationLimitBootAmountList.muflisBootAmount[n].GetComponent<GetBootAmount>().bootAmountText.text = GameManager.Instance.format.FormatNumber(long.Parse(tableData.response[i].bootAmount[n]));
						}
					}
					else if (tableData.response[i].type == "variation_nolimit")
					{
						for (int num = 0; num < tableData.response[i].bootAmount.Length; num++)
						{
							URL.Instance.DebugPrint("variation_nolimit : " + tableData.response[i].bootAmount[num]);
							variationNoLimitBootAmountList.muflisNoLimitBootAmount[num].SetActive(value: true);
							variationNoLimitBootAmountList.muflisNoLimitBootAmount[num].GetComponent<GetBootAmount>().bootAmount = tableData.response[i].bootAmount[num];
							variationNoLimitBootAmountList.muflisNoLimitBootAmount[num].GetComponent<GetBootAmount>().bootAmountText.text = GameManager.Instance.format.FormatNumber(long.Parse(tableData.response[i].bootAmount[num]));
						}
					}
				}
			}
			GameManager.Instance.loadingCanvas.LoadingState(false, false);
			if (GameManager.Instance.VipDetails != 0)
			{
				int num2 = UnityEngine.Random.Range(0, 5);
				if (num2 % 4 == 0)
				{
					URL.Instance.DebugPrint("Yo");
					OpenDealsMenu();
				}
			}
			else if (GameManager.Instance.isVIP)
			{
				vipDetailsGO.OpenPopup();
			}
			if (GameManager.Instance.isBootedOut)
			{
				GameManager.Instance.LanguageConverter(bootedOutBody, "ApnI bajI ims krne pr Aapkae $ebl se h$aya gya hW -", GameManager.Instance.bootedOutBody);
				GameManager.Instance.LanguageConverter(bootedOutTitle, "h$a idya gya -", GameManager.Instance.bootedOutTitle);
				bootedOutPopup.OpenPopup();
			}
			else
			{
				bootedOutPopup.ClosePopup();
			}
		}, delegate(Exception ex)
		{
			UnityEngine.Debug.LogException(ex);
			NativeUI.ShowToast("Oh Snap! Something went wrong :(", isLongToast: true);
		});
	}

	public void JoinPrivateTableButton()
	{
		GameManager.Instance.loadingCanvas.LoadingState(_isActive: true, _tip: false);
		if (privateTableIF.text.Length != 0)
		{
			JSONObject jSONObject = new JSONObject();
			jSONObject.AddField("playerId", GameManager.Instance.localPlayerID);
			jSONObject.AddField("code", privateTableIF.text);
			ObservableWWW.Get(URL.Instance.CALLAPI(URL.Instance.PRIVATE_TABLE_JOIN, jSONObject.ToString())).Subscribe(delegate(string x)
			{
				URL.Instance.DebugPrint("PRIVATE_TABLE_JOIN " + GameManager.Instance.security.Decrypt(x));
				SocketData socketData = SocketData.CreateFromJSON(GameManager.Instance.security.Decrypt(x));
				if (socketData.status == "success")
				{
					GameManager.Instance.host = socketData.response.host;
					GameManager.Instance.port = socketData.response.port;
					GameManager.Instance.tbid = socketData.response.tbid;
					SceneManager.LoadScene("TableScene");
				}
				else
				{
					NativeUI.ShowToast(socketData.popupMessageBody, isLongToast: true);
					GameManager.Instance.loadingCanvas.LoadingState(_isActive: false, _tip: false);
				}
				GameManager.Instance.loadingCanvas.LoadingState(_isActive: false, _tip: false);
			}, delegate(Exception ex)
			{
				UnityEngine.Debug.LogException(ex);
				NativeUI.ShowToast("Oh Snap! Something went wrong :(", isLongToast: true);
				GameManager.Instance.loadingCanvas.LoadingState(_isActive: false, _tip: false);
			});
		}
		else
		{
			URL.Instance.DebugPrint("Please enter code...");
			NativeUI.ShowToast("Please enter code...", isLongToast: true);
			GameManager.Instance.loadingCanvas.LoadingState(_isActive: false, _tip: false);
		}
	}

	private void Update()
	{
        if (Input.GetKeyDown(KeyCode.V))
        {
			DailyBonus();
        }

		if (startBonusTimer && bonusTimer > 0f)
		{
			bonusTimer -= Time.deltaTime;
			GameManager.Instance.lastbonusTime = bonusTimer;
			TimeSpan timeSpan = TimeSpan.FromSeconds(bonusTimer);
			string text = $"{timeSpan.Hours:D2}h:{timeSpan.Minutes:D2}m:{timeSpan.Seconds:D2}s";
			bonusTimerText.text = text;
			bonusTimerButton.interactable = false;
		}
		else
		{
			GameManager.Instance.LanguageConverter(bonusTimerText, "bons", "Bonus");
			bonusTimerButton.interactable = true;
		}
		if (UnityEngine.Input.GetKeyDown(KeyCode.Escape))
		{
			OpenExitPopup();
		}
	}

	public void OpenSettingsPanel()
	{
		GameManager.Instance.OpenSettingCanvas();
	}

	public void OpenSlotsScene()
	{
		AudioManager.instance.PlaySound("click");
		GameManager.Instance.loadingCanvas.LoadingState(_isActive: true, _tip: false);
		SceneManager.LoadScene(URL.Instance.SLOTS_SCENE);
	}

	public void OpenExitPopup()
	{
		exitPopup.OpenPopup();
	}

	public void OpenWatchAdsPopup()
	{
		watchAdsPopup.OpenPopup();
	}

	public void CloseWatchAdsPopup()
	{
		watchAdsPopup.ClosePopup();
	}

	public void ExitGame()
	{
		Application.Quit();
	}

	public void RequestRewardedVideo()
	{
		//HZIncentivizedAd.Fetch();
	}

	public void ShowRewardedVideo()
	{
		/*if (HZIncentivizedAd.IsAvailable())
		{
			HZIncentivizedAd.Show();
		}
		else
		{
			NativeUI.ShowToast("Ads not available. Please try again after some time.");
		}*/
		RequestRewardedVideo();
	}

	public void RewardedVideoCallback()
	{
		/*HZIncentivizedAd.AdDisplayListener displayListener = delegate(string adState, string adTag)
		{
			if (adState.Equals("incentivized_result_complete"))
			{
				VideoReward();
			}
			if (adState.Equals("incentivized_result_incomplete"))
			{
				NativeUI.ShowToast("You did not watch the entire video and should not be given a reward.");
			}
		};
		HZIncentivizedAd.SetDisplayListener(displayListener);*/
	}
}
