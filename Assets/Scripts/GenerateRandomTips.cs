using TMPro;
using UnityEngine;

public class GenerateRandomTips : MonoBehaviour
{
	private TextMeshProUGUI tipText;

	private string[] tipMessages = new string[5];

	private void Awake()
	{
		tipText = GetComponent<TextMeshProUGUI>();
		tipMessages[0] = "Earn free chips by watching Ads.";
		tipMessages[1] = "Multiply your chips by upto 10x in Slots.";
		tipMessages[2] = "If you are low in chips, try your luck in limit table.";
		tipMessages[3] = "Daily free bonus in every 4 hours.";
		tipMessages[4] = "Purchase Deals and it will save your money.";
	}

	private void OnEnable()
	{
		tipText.text = tipMessages[Random.Range(0, 5)];
	}
}
