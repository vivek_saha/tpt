public class OSNotification
{
	public enum DisplayType
	{
		Notification,
		InAppAlert,
		None
	}

	public bool isAppInFocus;

	public bool shown;

	public bool silentNotification;

	public int androidNotificationId;

	public DisplayType displayType;

	public OSNotificationPayload payload;
}
