using UnityEngine;

public class State_Force_Side_Show : MonoBehaviour
{
	[HideInInspector]
	public PlayerElements playerElements;

	private void Awake()
	{
		playerElements = GetComponent<PlayerElements>();
	}

	public void ForceSideShowSender()
	{
		if (playerElements.isActive)
		{
			playerElements.isTimerRunning = false;
			playerElements.firstTimer = false;
			playerElements.secondTimer = false;
			playerElements.ResetPlayerTimer();
			playerElements.ResetColor();
			if (playerElements.isLocalPlayer)
			{
				playerElements.tElements.NotMyTurn();
			}
		}
	}

	public void ForceSideShowReceiver()
	{
		if (playerElements.isActive)
		{
			playerElements.isTimerRunning = false;
			playerElements.firstTimer = false;
			playerElements.secondTimer = false;
			playerElements.ResetPlayerTimer();
			playerElements.ResetColor();
			if (playerElements.isLocalPlayer)
			{
				playerElements.tElements.NotMyTurn();
			}
		}
	}

	public void ForceSideShowWinner()
	{
		URL.Instance.DebugPrint("Force Winner");
	}
}
