using EasyMobile;
using Facebook.Unity;
using System.Collections;
using System.IO;
using System.Runtime.CompilerServices;
using TMPro;
using UniRx;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.XR;

public class GameManager : MonoBehaviour
{
	public static GameManager Instance;

	[Header("Version Number")]
	public string versionNumber;

	[Header("Debuging")]
	public bool isDebuging;

	[Header("Socket Details")]
	public string host;

	public string port;

	public string tbid;

	public string type;

	public string tblBootedAmount;

	public bool isSocketConnectedToServer;

	public double internetSpeed;

	public bool isConnectedToInternet;

	[Header("Hindi Font Asset")]
	public TMP_FontAsset hindiFont;

	public TMP_FontAsset englishFont;

	public Font defaulthindiFont;

	public Font defaultenglishFont;

	[Header("Local Player Details")]
	public string localPlayerName;

	public string id;

	public string localPlayerID;

	public string tempImage;

	public string tempName;

	public string fbChips;

	public bool isBlind;

	public bool isVIP;

	public long localPlayerChips;

	public long localPlayerForceSideshow;

	public long localPlayerPosition;

	public long tempLocalPlayerPosition;

	public string notificationID;

	public float lastbonusTime;

	public long forceDeduct;

	public bool isFacebookLogin;

	public bool isSwitchingTable;

	public long maxSlotsChaalAmount;

	public string slotPair;

	public string slotTrail;

	public string slotPureSeq;

	public string slotSeq;

	public string slotColor;

	public bool isBootedOut;

	public string bootedOutTitle;

	public string bootedOutBody;

	public int VipDetails;

	public string pinkyURL;

	public int promotionalImageDisplay;

	public bool canDoAutoLogin;

	[Header("Profile Sprites")]
	public Sprite[] profileDummyImages;

	[Header("Second Player Name")]
	public string secondPlayerNameForGifts;

	[Header("Counting Elements")]
	public float chipCountDuration = 1f;

	public long tempTotalAmountOnTable;

	[Header("Setting Canvas")]
	public GameObject settingCanvas;

	[HideInInspector]
	public NumberFormat format;

	[HideInInspector]
	public LoadingCanvas loadingCanvas;

	[HideInInspector]
	public InAppValues inAppValuesCanvas;

	[HideInInspector]
	public Settings sCanvas;

	[HideInInspector]
	public LoginManager loginManager;

	[HideInInspector]
	public EncDec security;

	public bool canDeviceCheck;

	/*[CompilerGenerated]
	private static OneSignal.NotificationOpened _003C_003Ef__mg_0024cache0;*/

	private void Awake()
	{
		if (Instance == null)
		{
			Instance = this;
		}
		else if (Instance != this)
		{
			Destroy(base.gameObject);
		}
		DontDestroyOnLoad(base.gameObject);
		if (!FB.IsInitialized)
		{
			FB.Init(InitCallback, OnHideUnity);
		}
		else
		{
			FB.ActivateApp();
		}
		XRSettings.enabled = false;
		Application.targetFrameRate = 60;
		Screen.sleepTimeout = -1;
		format = GetComponent<NumberFormat>();
		loadingCanvas = GetComponent<LoadingCanvas>();
		inAppValuesCanvas = GetComponent<InAppValues>();
		security = GetComponent<EncDec>();
		sCanvas = GetComponent<Settings>();
		loginManager = GetComponent<LoginManager>();
		notificationID = "0";
		isConnectedToInternet = true;
	}

	private void Start()
	{
		InvokeRepeating("CheckDeviceConnection", 5f, 5f);
		//OneSignal.StartInit("07d03a8e-dd4c-45ef-9a3b-480f1210ab61").HandleNotificationOpened(HandleNotificationOpened).EndInit();
		//OneSignal.inFocusDisplayType = OneSignal.OSInFocusDisplayOption.Notification;
		//OneSignal.permissionObserver += OneSignal_permissionObserver;
		//OSPermissionSubscriptionState permissionSubscriptionState = OneSignal.GetPermissionSubscriptionState();
		//notificationID = permissionSubscriptionState.subscriptionStatus.userId;
		//OneSignal.subscriptionObserver += OneSignal_subscriptionObserver;
		PlayerPrefs.GetFloat("Facebook", 0f);
		promotionalImageDisplay = 1;
	}

	private static void HandleNotificationOpened(OSNotificationOpenedResult result)
	{
		NativeUI.ShowToast(result.notification.payload.body);
	}

	private void OneSignal_permissionObserver(OSPermissionStateChanges stateChanges)
	{
		if (stateChanges.from.status == OSNotificationPermission.NotDetermined)
		{
			if (stateChanges.to.status == OSNotificationPermission.Authorized)
			{
				UnityEngine.Debug.Log("Thanks for accepting notifications!");
				NativeUI.ShowToast("Thanks for accepting notifications!");
			}
			else if (stateChanges.to.status == OSNotificationPermission.Denied)
			{
				UnityEngine.Debug.Log("Notifications not accepted. You can turn them on later under your device settings.");
				NativeUI.ShowToast("Notifications not accepted. You can turn them on later under your device settings.");
			}
		}
		UnityEngine.Debug.Log("stateChanges.to.status: " + stateChanges.to.status);
	}

	public void LanguageConverter(TextMeshProUGUI _text, string _hindi, string _english)
	{
		if (PlayerPrefs.GetInt("Language") == 1)
		{
			_text.font = hindiFont;
			_text.text = _hindi;
		}
		else
		{
			_text.text = _english;
		}
	}

	public void LanguageConverter(Text _text, string _hindi, string _english)
	{
		if (PlayerPrefs.GetInt("Language") == 1)
		{
			_text.font = defaulthindiFont;
			_text.text = _hindi;
		}
		else
		{
			_text.text = _english;
		}
	}

	private void OneSignal_subscriptionObserver(OSSubscriptionStateChanges stateChanges)
	{
		UnityEngine.Debug.Log("stateChanges: " + stateChanges);
		UnityEngine.Debug.Log("stateChanges.to.userId: " + stateChanges.to.userId);
		notificationID = stateChanges.to.userId;
		UnityEngine.Debug.Log("stateChanges.to.subscribed: " + stateChanges.to.subscribed);
	}

	private void InitCallback()
	{
		if (FB.IsInitialized)
		{
			FB.ActivateApp();
		}
		else
		{
			UnityEngine.Debug.Log("Failed to Initialize the Facebook SDK");
		}
	}

	private void OnHideUnity(bool isGameShown)
	{
		if (!isGameShown)
		{
			Time.timeScale = 0f;
		}
		else
		{
			Time.timeScale = 1f;
		}
	}

	public void CountChips(long _prev, long _target, TextMeshProUGUI _text, long _amount)
	{
		StartCoroutine(CountTo(_prev, _target, _text, _amount));
	}

	private IEnumerator CountTo(long _prev, long _target, TextMeshProUGUI _text, long _amount)
	{
		for (float timer = 0f; timer < chipCountDuration; timer += Time.deltaTime)
		{
			float progress = timer / chipCountDuration;
			_amount = (long)Mathf.Lerp(_prev, _target, progress);
			_text.text = format.FormatNumberOnTable(_amount);
			yield return null;
		}
		_amount = _target;
		tempTotalAmountOnTable = _amount;
		_text.text = format.FormatNumberOnTable(_amount);
	}

	public void CountChipsSinglePlayer(long _prev, long _target, TextMeshProUGUI _text)
	{
		StartCoroutine(CountToSinglePlayer(_prev, _target, _text));
	}

	private IEnumerator CountToSinglePlayer(long _prev, long _target, TextMeshProUGUI _text)
	{
		for (float timer = 0f; timer < chipCountDuration; timer += Time.deltaTime)
		{
			float progress = timer / chipCountDuration;
			long tempChips = (long)Mathf.Lerp(_prev, _target, progress);
			_text.text = format.FormatNumber(tempChips);
			yield return null;
		}
		_text.text = format.FormatNumber(_target);
	}

	public void ResetGameManagerAfterEveryGame()
	{
		tempTotalAmountOnTable = 0L;
	}

	public void OpenSettingCanvas()
	{
		AudioManager.instance.PlaySound("click");
		settingCanvas.GetComponent<Animator>().SetBool("isopen", value: true);
	}

	public void CloseSettingCanvas()
	{
		AudioManager.instance.PlaySound("close-popup");
		settingCanvas.GetComponent<Animator>().SetBool("isopen", value: false);
	}

	public void CheckDeviceConnection()
	{
		if (!(localPlayerID == string.Empty))
		{
			JSONObject jSONObject = new JSONObject();
			jSONObject.AddField("playerId", localPlayerID);
			ObservableWWW.Get(URL.Instance.CALLAPI(URL.Instance.MULTI_LOGIN, jSONObject.ToString())).Subscribe(delegate(string x)
			{
				PlayerData playerData = PlayerData.CreateFromJSON(security.Decrypt(x));
				if (playerData.status == "success")
				{
					if (playerData.response.deviceId != SystemInfo.deviceUniqueIdentifier)
					{
						if (canDeviceCheck)
						{
							canDeviceCheck = false;
							URL.Instance.DebugPrint("Another Device");
							NativeUI.ShowToast("Sorry! You've been kicked out.As you've logged in from another device");
							PlayerPrefs.SetFloat("Facebook", 0f);
							SceneManager.LoadScene(URL.Instance.LOGIN_SCENE);
						}
					}
					else
					{
						canDeviceCheck = true;
					}
				}
			});
		}
	}

	public void LogMessage(string _message)
	{
		//GoogleAnalyticsV4.instance.LogScreen("Android : " + _message);
	}

	public void LoadImagesCO(Image _profileImage, string _name)
	{
		if (File.Exists(ImageName(_name)))
		{
			byte[] array = File.ReadAllBytes(ImageName(_name));
			if ((array[0] == 137 && array[1] == 80 && array[2] == 78 && array[3] == 71 && array[4] == 13 && array[5] == 10 && array[6] == 26 && array[7] == 10) || (array[0] == byte.MaxValue && array[1] == 216 && array[2] == byte.MaxValue && array[3] == 224 && array[4] == 0 && array[5] == 16 && array[6] == 74 && array[7] == 70 && array[8] == 73 && array[9] == 70))
			{
				Texture2D texture2D = new Texture2D(8, 8);
				texture2D.LoadImage(array);
				Sprite sprite2 = _profileImage.sprite = Sprite.Create(texture2D, new Rect(0f, 0f, texture2D.width, texture2D.height), Vector2.zero);
				URL.Instance.DebugPrint("Image already downloaded : " + _name);
			}
			else
			{
				URL.Instance.DebugPrint("Image error downloaded : " + _name);
			}
		}
		else
		{
			URL.Instance.DebugPrint("Profile Image not found : " + _name);
		}
	}

	private string ImageName(string _name)
	{
		return Path.Combine(path2: "profile_image_" + (23 - int.Parse(_name)).ToString() + ".png", path1: Application.persistentDataPath);
	}
}
