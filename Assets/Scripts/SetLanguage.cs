using EasyMobile;
using UnityEngine;
using UnityEngine.UI;

public class SetLanguage : MonoBehaviour
{
	public Toggle engToggle;

	public Toggle hiToggle;

	private void Start()
	{
		if (PlayerPrefs.GetInt("Language") == 1)
		{
			hiToggle.isOn = true;
			engToggle.isOn = false;
			engToggle.onValueChanged.AddListener(delegate
			{
				OnCLickToggle();
			});
		}
		else
		{
			hiToggle.isOn = false;
			engToggle.isOn = true;
			hiToggle.onValueChanged.AddListener(delegate
			{
				OnCLickToggle();
			});
		}
	}

	public void OnCLickToggle()
	{
		NativeUI.AlertPopup alertPopup = NativeUI.ShowTwoButtonAlert("Notice.", "You will need to restart the app if you switch languages. Continue?", "Yes", "No");
		if (alertPopup != null)
		{
			alertPopup.OnComplete += OnAlertCompleteHandler;
		}
	}

	private void OnAlertCompleteHandler(int buttonIndex)
	{
		switch (buttonIndex)
		{
		case 0:
			if (hiToggle.isOn)
			{
				PlayerPrefs.SetInt("Language", 1);
			}
			else
			{
				PlayerPrefs.SetInt("Language", 0);
			}
			Application.Quit();
			break;
		case 1:
			hiToggle.onValueChanged.RemoveAllListeners();
			engToggle.onValueChanged.RemoveAllListeners();
			if (PlayerPrefs.GetInt("Language") == 1)
			{
				hiToggle.isOn = true;
				engToggle.isOn = false;
				engToggle.onValueChanged.AddListener(delegate
				{
					OnCLickToggle();
				});
			}
			else
			{
				hiToggle.isOn = false;
				engToggle.isOn = true;
				hiToggle.onValueChanged.AddListener(delegate
				{
					OnCLickToggle();
				});
			}
			break;
		}
	}
}
