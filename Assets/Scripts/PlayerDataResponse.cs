using System;
using UnityEngine;

[Serializable]
public class PlayerDataResponse
{
	public string pinkyURL;

	public string playerId;

	public string chips;

	public bool sittingTable;

	public bool isActive;

	public bool hasCards;

	public int position;

	public int turn;

	public bool isBlind;

	public string[] cards;

	public string amount;

	public string timer;

	public long totalAmount;

	public string msg;

	public int blind;

	public string chaalAmountLimit;

	public string bootedAmountChips;

	public string tableAmountLimit;

	public string type;

	public string forcesideshow;

	public string join_number;

	public string variation;

	public string user_id;

	public string facebook;

	public string name;

	public string uid;

	public string tempname;

	public string timediff;

	public string lastbonus;

	public string reward;

	public string tbid;

	public string host;

	public string port;

	public string proto;

	public string winChips;

	public string version;

	public string seconds;

	public string rank;

	public string winAmount;

	public string profile;

	public string deviceId;

	public string vip;

	public string maxSlotChaalAmount;

	public string vipDetail;

	public string fbChips;

	public string imageUrl;

	public string promotionalURLString;

	public string fbMultiMsg;

	public SlotsDataResponse slotRewards;

	public static PlayerDataResponse CreateFromJSON(string data)
	{
		return JsonUtility.FromJson<PlayerDataResponse>(data);
	}
}
