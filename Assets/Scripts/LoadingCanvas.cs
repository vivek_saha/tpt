using UnityEngine;

public class LoadingCanvas : MonoBehaviour
{
	[SerializeField]
	private GameObject loadingGO;

	[SerializeField]
	private GameObject loadingGOWithoutTip;

	private void Start()
	{
		LoadingState(_isActive: false, _tip: false);
	}

	public void LoadingState(bool _isActive, bool _tip)
	{
		if (_isActive)
		{
			if (_tip)
			{
				loadingGO.SetActive(value: true);
				loadingGOWithoutTip.SetActive(value: false);
			}
			else
			{
				loadingGO.SetActive(value: false);
				loadingGOWithoutTip.SetActive(value: true);
			}
		}
		else
		{
			loadingGO.SetActive(value: false);
			loadingGOWithoutTip.SetActive(value: false);
		}
	}
}
