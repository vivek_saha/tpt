using System;
using UnityEngine;

[Serializable]
public class SlotsDataResponse
{
	public string slotTrail;

	public string slotPureSeq;

	public string slotSeq;

	public string slotColor;

	public string slotPair;

	public static SlotsDataResponse CreateFromJSON(string data)
	{
		return JsonUtility.FromJson<SlotsDataResponse>(data);
	}
}
