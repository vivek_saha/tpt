using System.Collections;
using System.Collections.Generic;
using System.IO;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LocalizationManager : MonoBehaviour
{
	public static LocalizationManager instance;

	private string missingLocalizedTextString = "Key not found";

	private Dictionary<string, string> localizedText;

	private bool isReady;

	public TMP_FontAsset _hiFont;

	public TMP_FontAsset _enFont;

	public bool isHindi;

	private void Awake()
	{
		if (instance == null)
		{
			instance = this;
		}
		else if (instance != this)
		{
			UnityEngine.Object.Destroy(base.gameObject);
		}
		Object.DontDestroyOnLoad(base.gameObject);
	}

	public void LoadLocalizedText(string fileName)
	{
		localizedText = new Dictionary<string, string>();
		string path = Path.Combine(Application.streamingAssetsPath, fileName);
		if (fileName == "localisedText_hi.json")
		{
			isHindi = true;
		}
		else
		{
			isHindi = false;
		}
		if (File.Exists(path))
		{
			string json = File.ReadAllText(path);
			LocalizationData localizationData = JsonUtility.FromJson<LocalizationData>(json);
			for (int i = 0; i < localizationData.items.Length; i++)
			{
				localizedText.Add(localizationData.items[i].key, localizationData.items[i].value);
			}
			UnityEngine.Debug.Log("data loaded : " + localizedText.Count);
			StartCoroutine(LoadMainMenuScene());
		}
		else
		{
			UnityEngine.Debug.LogError("Cannot Find File!!");
		}
		isReady = true;
	}

	private IEnumerator LoadMainMenuScene()
	{
		while (!instance.GetIsReady())
		{
			yield return null;
		}
		SceneManager.LoadScene("LoginScene");
	}

	public string GetLocalizedValue(string key)
	{
		string result = missingLocalizedTextString;
		if (localizedText.ContainsKey(key))
		{
			result = localizedText[key];
		}
		return result;
	}

	public bool GetIsReady()
	{
		return isReady;
	}
}
