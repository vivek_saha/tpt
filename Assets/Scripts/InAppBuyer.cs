using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class InAppBuyer : MonoBehaviour
{
	private InAppValues inApp;

	public string packageName;

	public Text priceText;

	public TextMeshProUGUI chipsText;

	public TextMeshProUGUI discountText;

	public GameObject[] offerGO;

	private void Start()
	{
		inApp = GetComponentInParent<InAppValues>();
	}

	public void InAppBuy()
	{
		if (packageName.Length != 0 && packageName != null)
		{
			inApp.BuyInApp(packageName);
		}
	}

	public void DisplayOffers(string _num)
	{
		if (int.Parse(_num) <= 2)
		{
			offerGO[int.Parse(_num)].SetActive(value: true);
			return;
		}
		offerGO[0].SetActive(value: false);
		offerGO[1].SetActive(value: false);
		offerGO[2].SetActive(value: false);
	}
}
