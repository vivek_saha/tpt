using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadLocalizations : MonoBehaviour
{
	private void Start()
	{
		GameManager.Instance.loadingCanvas.LoadingState(false, false);
	}

	public void LoadEnglish()
	{
		GameManager.Instance.loadingCanvas.LoadingState(true, false);
		PlayerPrefs.SetInt("Language", 0);
		SceneManager.LoadScene("LoginScene");
	}

	public void LoadHindi()
	{
		GameManager.Instance.loadingCanvas.LoadingState(true, false);
		PlayerPrefs.SetInt("Language", 1);
		SceneManager.LoadScene("LoginScene");
	}
}
