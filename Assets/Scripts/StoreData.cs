using System;
using UnityEngine;

[Serializable]
public class StoreData
{
	public string Store;

	public string TransactionID;

	public StoreDataResponse Payload;

	public static StoreData CreateFromJSON(string data)
	{
		return JsonUtility.FromJson<StoreData>(data);
	}
}
