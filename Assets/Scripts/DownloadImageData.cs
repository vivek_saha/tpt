using System;
using UnityEngine;

[Serializable]
public class DownloadImageData
{
	public string status;

	public string responseMessage;

	public string popupMessageTitle;

	public string popupMessageBody;

	public DownloadImageResponse[] response;

	public static DownloadImageData CreateFromJSON(string data)
	{
		return JsonUtility.FromJson<DownloadImageData>(data);
	}
}
