using System.Collections;
using UnityEngine;

public class StartupManager : MonoBehaviour
{
	public GameObject localisedScreen;

	public GameObject mainMenuScreen;

	private IEnumerator Start()
	{
		while (!LocalizationManager.instance.GetIsReady())
		{
			yield return null;
		}
		localisedScreen.SetActive(value: false);
		mainMenuScreen.SetActive(value: true);
	}
}
