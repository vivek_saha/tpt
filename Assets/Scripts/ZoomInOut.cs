using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class ZoomInOut : MonoBehaviour
{
	public Image img;

	private void Start()
	{
		img.transform.DOScale(Vector3.zero, 1f).SetLoops(-1, LoopType.Yoyo);
	}
}
