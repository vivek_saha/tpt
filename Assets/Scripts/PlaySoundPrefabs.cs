using UnityEngine;

public class PlaySoundPrefabs : MonoBehaviour
{
	public string _name;

	private void Start()
	{
		AudioManager.instance.PlaySound(_name);
	}
}
