﻿using EasyMobile;
using System;
using System.Collections;
using TMPro;
using UniRx;
using UnityEngine;
//using UnityEngine.Purchasing;
//using UnityEngine.Purchasing.Security;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class InAppValues : MonoBehaviour
{
	[Header("In App Elements")]
	[SerializeField]
	private OpenClosePopup inAppGO;

	[SerializeField]
	private InAppBuyer[] inAppObjects;

	[SerializeField]
	private TextMeshProUGUI dealsChipsText;

	[SerializeField]
	private TextMeshProUGUI dealsForceText;

	[SerializeField]
	private Text dealsPriceText;

	[SerializeField]
	private OpenClosePopup dealsPanelGO;

	[SerializeField]
	private BuyDeals buyDeals;

	[Header("In App Complete")]
	public OpenClosePopup inappPopup;

	public TextMeshProUGUI chipsPurchasedText;

	public OpenClosePopup dealPopup;

	public TextMeshProUGUI chipsDealPurchasedText;

	public TextMeshProUGUI forcePurchasedText;

	private string deal1Price;

	private string deal2Price;

	private string deal3Price;

	private string deal1Price_VIP;

	private string deal2Price_VIP;

	private string deal3Price_VIP;

	[Header("VIP")]
	public OpenClosePopup vipGO;

	//private static IStoreController m_StoreController;

	//private static IExtensionProvider m_StoreExtensionProvider;

	public static string pack_1 = "com.starlabs.teenpattitycoon.pack1";

	public static string pack_2 = "com.starlabs.teenpattitycoon.pack2";

	public static string pack_3 = "com.starlabs.teenpattitycoon.pack3";

	public static string pack_4 = "com.starlabs.teenpattitycoon.pack4";

	public static string pack_5 = "com.starlabs.teenpattitycoon.pack5";

	public static string pack_6 = "com.starlabs.teenpattitycoon.pack6";

	public static string pack_7 = "com.starlabs.teenpattitycoon.pack7";

	public static string pack_8 = "com.starlabs.teenpattitycoon.pack8";

	public static string pack_9 = "com.starlabs.teenpattitycoon.pack9";

	public static string pack_10 = "com.starlabs.teenpattitycoon.pack10";

	public static string pack_11 = "com.starlabs.teenpattitycoon.pack11";

	public static string deal1 = "com.starlabs.teenpattitycoon.deal1";

	public static string deal2 = "com.starlabs.teenpattitycoon.deal2";

	public static string deal3 = "com.starlabs.teenpattitycoon.deal3";

	public static string deal1_vip = "com.starlabs.teenpattitycoon.deal1_vip";

	public static string deal2_vip = "com.starlabs.teenpattitycoon.deal2_vip";

	public static string deal3_vip = "com.starlabs.teenpattitycoon.deal3_vip";

	private bool isDealBuying;

	private void Awake()
	{
		for (int i = 0; i < inAppObjects.Length; i++)
		{
			inAppObjects[i].gameObject.SetActive(value: false);
		}
	}

	private void Start()
	{
		/*if (m_StoreController == null)
		{
			InitializePurchasing();
		}
		else
		{
			NativeUI.Alert("Oh Snap!", "Something is wrong with In-App purchases.", "OK");
		}*/
	}

	public void PackageListing()
	{
		GameManager.Instance.loadingCanvas.LoadingState(_isActive: true, _tip: false);
		ObservableWWW.Get(URL.Instance.CALLAPI(URL.Instance.PACKAGE_LISTING, string.Empty)).Subscribe(delegate(string x)
		{
			URL.Instance.DebugPrint("PackageListingCO " + GameManager.Instance.security.Decrypt(x));
			DealsData dealsData = DealsData.CreateFromJSON(GameManager.Instance.security.Decrypt(x));
			if (dealsData.status == "success")
			{
				for (int i = 0; i < dealsData.response.Length; i++)
				{
					if (dealsData.response.Length <= inAppObjects.Length)
					{
						inAppObjects[i].packageName = dealsData.response[i].productid_android;
						inAppObjects[i].chipsText.text = GameManager.Instance.format.FormatNumberOnTable(long.Parse(dealsData.response[i].chips));
						inAppObjects[i].discountText.text = dealsData.response[i].discount;
						inAppObjects[i].DisplayOffers(dealsData.response[i].promoType);
						inAppObjects[i].gameObject.SetActive(value: true);
					}
				}
				inAppGO.OpenPopup();
				GameManager.Instance.loadingCanvas.LoadingState(_isActive: false, _tip: false);
			}
		}, delegate(Exception ex)
		{
			UnityEngine.Debug.LogException(ex);
			NativeUI.ShowToast("Oh Snap! Something went wrong :(", isLongToast: true);
			GameManager.Instance.loadingCanvas.LoadingState(_isActive: false, _tip: false);
		});
	}

	public void InitializePurchasing()
	{
		/*if (!IsInitialized())
		{
			ConfigurationBuilder configurationBuilder = ConfigurationBuilder.Instance(StandardPurchasingModule.Instance());
			configurationBuilder.AddProduct(pack_1, ProductType.Consumable);
			configurationBuilder.AddProduct(pack_2, ProductType.Consumable);
			configurationBuilder.AddProduct(pack_3, ProductType.Consumable);
			configurationBuilder.AddProduct(pack_4, ProductType.Consumable);
			configurationBuilder.AddProduct(pack_5, ProductType.Consumable);
			configurationBuilder.AddProduct(pack_6, ProductType.Consumable);
			configurationBuilder.AddProduct(pack_7, ProductType.Consumable);
			configurationBuilder.AddProduct(pack_8, ProductType.Consumable);
			configurationBuilder.AddProduct(pack_9, ProductType.Consumable);
			configurationBuilder.AddProduct(pack_10, ProductType.Consumable);
			configurationBuilder.AddProduct(pack_11, ProductType.Consumable);
			configurationBuilder.AddProduct(deal1, ProductType.Consumable);
			configurationBuilder.AddProduct(deal2, ProductType.Consumable);
			configurationBuilder.AddProduct(deal3, ProductType.Consumable);
			configurationBuilder.AddProduct(deal1_vip, ProductType.Consumable);
			configurationBuilder.AddProduct(deal2_vip, ProductType.Consumable);
			configurationBuilder.AddProduct(deal3_vip, ProductType.Consumable);
			UnityPurchasing.Initialize(this, configurationBuilder);
		}*/
	}

	/*private bool IsInitialized()
	{
		return m_StoreController != null && m_StoreExtensionProvider != null;
	}*/

	private void BuyProductID(string productId)
	{
		/*if (IsInitialized())
		{
			Product product = m_StoreController.products.WithID(productId);
			if (product != null && product.availableToPurchase)
			{
				URL.Instance.DebugPrint($"Purchasing product asychronously: '{product.definition.id}'");
				m_StoreController.InitiatePurchase(product);
			}
			else
			{
				UnityEngine.Debug.Log("BuyProductID: FAIL. Not purchasing product, either is not found or is not available for purchase");
			}
		}
		else
		{
			UnityEngine.Debug.Log("BuyProductID FAIL. Not initialized.");
		}*/
	}

	/*public void OnInitialized(IStoreController controller, IExtensionProvider extensions)
	{
		m_StoreController = controller;
		m_StoreExtensionProvider = extensions;
	}

	public void OnInitializeFailed(InitializationFailureReason error)
	{
		UnityEngine.Debug.Log("OnInitializeFailed InitializationFailureReason:" + error);
		NativeUI.Alert("InApp Failed", error.ToString());
	}

	public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs args)
	{
		bool flag = true;
		CrossPlatformValidator crossPlatformValidator = new CrossPlatformValidator(GooglePlayTangle.Data(), AppleTangle.Data(), Application.identifier);
		try
		{
			IPurchaseReceipt[] array = crossPlatformValidator.Validate(args.purchasedProduct.receipt);
			URL.Instance.DebugPrint("Receipt is valid. Contents:");
			IPurchaseReceipt[] array2 = array;
			foreach (IPurchaseReceipt purchaseReceipt in array2)
			{
				URL.Instance.DebugPrint(purchaseReceipt.productID);
				URL.Instance.DebugPrint(purchaseReceipt.purchaseDate);
				URL.Instance.DebugPrint(purchaseReceipt.transactionID);
			}
		}
		catch (IAPSecurityException)
		{
			URL.Instance.DebugPrint("Invalid receipt, not unlocking content");
			NativeUI.Alert("Transaction Failed", "Invalid Purchase...");
			flag = false;
		}
		if (flag)
		{
			URL.Instance.DebugPrint("Purchased : " + args.purchasedProduct.definition.id);
			if (!isDealBuying)
			{
				BuyPackage(args.purchasedProduct.definition.id, args.purchasedProduct.receipt, m_StoreController.products.WithID(args.purchasedProduct.definition.id).metadata.localizedPriceString);
			}
			else
			{
				BuyDealPackage(args.purchasedProduct.definition.id, args.purchasedProduct.receipt, m_StoreController.products.WithID(args.purchasedProduct.definition.id).metadata.localizedPriceString);
			}
		}
		return PurchaseProcessingResult.Complete;
	}*/

	private IEnumerator BuyHack(string _id, string _receipt, string _price)
	{
		BuyPackage(_id, _receipt, _price);
		yield return new WaitForSeconds(3f);
		BuyPackage(_id, _receipt, _price);
	}

	/*public void OnPurchaseFailed(Product product, PurchaseFailureReason failureReason)
	{
		UnityEngine.Debug.Log($"OnPurchaseFailed: FAIL. Product: '{product.definition.storeSpecificId}', PurchaseFailureReason: {failureReason}");
		NativeUI.Alert("Transaction Failed", failureReason.ToString());
	}*/

	private void Update()
	{
		/*if (IsInitialized())
		{
			inAppObjects[0].priceText.text = m_StoreController.products.WithID(pack_1).metadata.localizedPriceString;
			inAppObjects[1].priceText.text = m_StoreController.products.WithID(pack_2).metadata.localizedPriceString;
			inAppObjects[2].priceText.text = m_StoreController.products.WithID(pack_3).metadata.localizedPriceString;
			inAppObjects[3].priceText.text = m_StoreController.products.WithID(pack_4).metadata.localizedPriceString;
			inAppObjects[4].priceText.text = m_StoreController.products.WithID(pack_5).metadata.localizedPriceString;
			inAppObjects[5].priceText.text = m_StoreController.products.WithID(pack_6).metadata.localizedPriceString;
			inAppObjects[6].priceText.text = m_StoreController.products.WithID(pack_7).metadata.localizedPriceString;
			inAppObjects[7].priceText.text = m_StoreController.products.WithID(pack_8).metadata.localizedPriceString;
			inAppObjects[8].priceText.text = m_StoreController.products.WithID(pack_9).metadata.localizedPriceString;
			inAppObjects[9].priceText.text = m_StoreController.products.WithID(pack_10).metadata.localizedPriceString;
			inAppObjects[10].priceText.text = m_StoreController.products.WithID(pack_11).metadata.localizedPriceString;
			deal1Price = m_StoreController.products.WithID(deal1).metadata.localizedPriceString;
			deal2Price = m_StoreController.products.WithID(deal2).metadata.localizedPriceString;
			deal3Price = m_StoreController.products.WithID(deal3).metadata.localizedPriceString;
			deal1Price_VIP = m_StoreController.products.WithID(deal1_vip).metadata.localizedPriceString;
			deal2Price_VIP = m_StoreController.products.WithID(deal2_vip).metadata.localizedPriceString;
			deal3Price_VIP = m_StoreController.products.WithID(deal3_vip).metadata.localizedPriceString;
		}*/
	}

	public void BuyInApp(string _id)
	{
		isDealBuying = false;
		BuyProductID(_id);
	}

	public void BuyDeals(string _id)
	{
		isDealBuying = true;
		BuyProductID(_id);
	}

	private void BuyPackage(string _packageName, string _orderID, string _price)
	{
		//args.purchasedProduct.definition.id,
		//args.purchasedProduct.receipt,
		//m_StoreController.products.WithID(args.purchasedProduct.definition.id).metadata.localizedPriceString

		GameManager.Instance.loadingCanvas.LoadingState(_isActive: true, _tip: false);
		URL.Instance.DebugPrint("InApp Print : " + _orderID);
		StoreData sData = StoreData.CreateFromJSON(_orderID);
		URL.Instance.DebugPrint(_orderID);
		JSONObject jSONObject = new JSONObject();
		jSONObject.AddField("playerId", GameManager.Instance.localPlayerID);
		jSONObject.AddField("package_name", _packageName);
		jSONObject.AddField("amount", _price);

		WWWForm wWWForm = new WWWForm();
		wWWForm.AddField("playerData", GameManager.Instance.security.Encrypt(jSONObject.ToString()));
		wWWForm.AddField("order_id", _orderID);
		URL.Instance.DebugPrint("In App : " + URL.Instance.CALLAPI(URL.Instance.PURCHASE_CHIPS, wWWForm.ToString()));
		ObservableWWW.Post(URL.Instance.CALLAPI(URL.Instance.PURCHASE_CHIPS), wWWForm).Subscribe(delegate(string x)
		{
			URL.Instance.DebugPrint("InApp Purchase : " + GameManager.Instance.security.Decrypt(x));
			TableResponseData tableResponseData = TableResponseData.CreateFromJSON(GameManager.Instance.security.Decrypt(x));
			if (tableResponseData.status == "success")
			{
				chipsPurchasedText.text = GameManager.Instance.format.FormatNumberOnTable(long.Parse(tableResponseData.response[0].reward));
				GameManager.Instance.localPlayerChips = long.Parse(tableResponseData.response[0].chips);
				if (SceneManager.GetActiveScene().name == URL.Instance.MAIN_MENU_SCENE)
				{
					TextMeshProUGUI component = GameObject.Find("ProfileChipsText").GetComponent<TextMeshProUGUI>();
					component.text = GameManager.Instance.format.FormatNumberOnTable(GameManager.Instance.localPlayerChips);
				}
				else if (SceneManager.GetActiveScene().name == URL.Instance.TABLE_SCENE)
				{
					GetPlayersWithPosition getPlayersWithPosition = FindObjectOfType<GetPlayersWithPosition>();
					getPlayersWithPosition._players[2].pController.pElements.chips = GameManager.Instance.localPlayerChips.ToString();
					getPlayersWithPosition._players[2].pController.pElements.chipsText.text = GameManager.Instance.format.FormatNumber(GameManager.Instance.localPlayerChips);
					if (long.Parse(getPlayersWithPosition._players[2].pController.pElements.amount) <= long.Parse(getPlayersWithPosition._players[2].pController.pElements.chips))
					{
						getPlayersWithPosition._players[2].pController.pElements.tElements.chaalButton.interactable = true;
						getPlayersWithPosition._players[2].pController.pElements.tElements.chaalButtonEvent.enabled = true;
						getPlayersWithPosition._players[2].pController.pElements.tElements.showButton.interactable = true;
						getPlayersWithPosition._players[2].pController.pElements.tElements.showButtonEvent.enabled = true;
						if (GameManager.Instance.localPlayerForceSideshow >= GameManager.Instance.forceDeduct)
						{
							getPlayersWithPosition._players[2].pController.pElements.tElements.forcesideshowButton.interactable = true;
							getPlayersWithPosition._players[2].pController.pElements.tElements.forcesideshowButtonEvent.enabled = true;
						}
						if (long.Parse(getPlayersWithPosition._players[2].pController.pElements.chips) >= long.Parse(getPlayersWithPosition._players[2].pController.pElements.amount) * 2)
						{
							getPlayersWithPosition._players[2].pController.pElements.tElements.increaseButton.interactable = true;
							getPlayersWithPosition._players[2].pController.pElements.tElements.decreaseButton.interactable = false;
						}
					}
				}
				if (!GameManager.Instance.isVIP && tableResponseData.response[0].newVip == "1")
				{
					vipGO.OpenPopup();
					GameManager.Instance.isVIP = true;
					if (SceneManager.GetActiveScene().name == URL.Instance.MAIN_MENU_SCENE)
					{
						FillMainMenu fillMainMenu = FindObjectOfType<FillMainMenu>();
						fillMainMenu.vipGO.SetActive(value: true);
					}
				}
				inAppGO.ClosePopup();
				inappPopup.OpenPopup();
				GameManager.Instance.LogMessage("InApp Purchased : " + _packageName + " Store : " + sData.Store);
			}
			else
			{
				NativeUI.Alert(tableResponseData.popupMessageTitle, tableResponseData.popupMessageBody);
			}
			GameManager.Instance.loadingCanvas.LoadingState(_isActive: false, _tip: false);
		}, delegate(Exception ex)
		{
			Debug.LogException(ex);
			NativeUI.ShowToast("Oh Snap! Something went wrong :(", isLongToast: true);
			GameManager.Instance.LogMessage("InApp Purchased Failed " + GameManager.Instance.localPlayerID);
			GameManager.Instance.loadingCanvas.LoadingState(_isActive: false, _tip: false);
		});
	}

	public void DealsListing()
	{
		GameManager.Instance.loadingCanvas.LoadingState(_isActive: true, _tip: false);
		JSONObject jSONObject = new JSONObject();
		jSONObject.AddField("playerId", GameManager.Instance.localPlayerID);
		ObservableWWW.Get(URL.Instance.CALLAPI(URL.Instance.DEALS_LISTING_API, jSONObject.ToString())).Subscribe(delegate(string x)
		{
			URL.Instance.DebugPrint("DEALS_LISTING_API " + GameManager.Instance.security.Decrypt(x));
			DealsData dealsData = DealsData.CreateFromJSON(GameManager.Instance.security.Decrypt(x));
			if (dealsData.status == "success")
			{
				dealsChipsText.text = GameManager.Instance.format.FormatNumberOnTable(long.Parse(dealsData.response[0].chips));
				dealsForceText.text = GameManager.Instance.format.FormatNumber(long.Parse(dealsData.response[0].forcesideshow));
				buyDeals.packageName = dealsData.response[0].package_name;
				buyDeals.packageID = dealsData.response[0].productid_android;
				if (buyDeals.packageName == deal1)
				{
					dealsPriceText.text = deal1Price;
				}
				else if (buyDeals.packageName == deal2)
				{
					dealsPriceText.text = deal2Price;
				}
				else if (buyDeals.packageName == deal3)
				{
					dealsPriceText.text = deal3Price;
				}
				else if (buyDeals.packageName == deal1_vip)
				{
					dealsPriceText.text = deal1Price_VIP;
				}
				else if (buyDeals.packageName == deal2_vip)
				{
					dealsPriceText.text = deal2Price_VIP;
				}
				else if (buyDeals.packageName == deal3_vip)
				{
					dealsPriceText.text = deal3Price_VIP;
				}
				dealsPanelGO.OpenPopup();
			}
		}, delegate(Exception ex)
		{
			Debug.LogException(ex);
			NativeUI.ShowToast("Oh Snap! Something went wrong :(", true);
		});
		GameManager.Instance.loadingCanvas.LoadingState(false, false);
	}

	public void CloseInAppPopup()
	{
		inAppGO.ClosePopup();
	}

	public void CloseDealsPopup()
	{
		dealsPanelGO.ClosePopup();
	}

	public void BuyDealPackage(string _packageName, string _orderID, string _price)
	{
		GameManager.Instance.loadingCanvas.LoadingState(true, false);
		StoreData sData = StoreData.CreateFromJSON(_orderID);
		JSONObject jSONObject = new JSONObject();
		jSONObject.AddField("playerId", GameManager.Instance.localPlayerID);
		jSONObject.AddField("package_name", _packageName);
		jSONObject.AddField("amount", _price);
		WWWForm wWWForm = new WWWForm();
		wWWForm.AddField("playerData", GameManager.Instance.security.Encrypt(jSONObject.ToString()));
		wWWForm.AddField("order_id", _orderID);
		ObservableWWW.Post(URL.Instance.CALLAPI(URL.Instance.DEAL_CHIPS), wWWForm).Subscribe(delegate(string x)
		{
			URL.Instance.DebugPrint("Deal Purchase : " + GameManager.Instance.security.Decrypt(x));
			TableResponseData tableResponseData = TableResponseData.CreateFromJSON(GameManager.Instance.security.Decrypt(x));
			if (tableResponseData.status == "success")
			{
				if (SceneManager.GetActiveScene().name == URL.Instance.MAIN_MENU_SCENE)
				{
					chipsDealPurchasedText.text = GameManager.Instance.format.FormatNumberOnTable(long.Parse(tableResponseData.response[0].reward));
					GameManager.Instance.localPlayerChips = long.Parse(tableResponseData.response[0].chips);
					forcePurchasedText.text = GameManager.Instance.format.FormatNumberOnTable(long.Parse(tableResponseData.response[0].rewardForceSideShow));
					GameManager.Instance.localPlayerForceSideshow = long.Parse(tableResponseData.response[0].forcesideshow);
					TextMeshProUGUI component = GameObject.Find("ProfileChipsText").GetComponent<TextMeshProUGUI>();
					component.text = GameManager.Instance.format.FormatNumberOnTable(GameManager.Instance.localPlayerChips);
					TextMeshProUGUI component2 = GameObject.Find("ProfileForceSideshowText").GetComponent<TextMeshProUGUI>();
					component2.text = GameManager.Instance.format.FormatNumberOnTable(GameManager.Instance.localPlayerForceSideshow);
					dealPopup.OpenPopup();
					dealsPanelGO.ClosePopup();
				}
				GameManager.Instance.LogMessage("Deal Purchased : " + _packageName + " Store : " + sData.Store);
			}
			else
			{
				NativeUI.Alert(tableResponseData.popupMessageTitle, tableResponseData.popupMessageBody);
			}
			GameManager.Instance.loadingCanvas.LoadingState(_isActive: false, _tip: false);
		}, delegate(Exception ex)
		{
			UnityEngine.Debug.LogException(ex);
			NativeUI.ShowToast("Oh Snap! Something went wrong :(", isLongToast: true);
			GameManager.Instance.LogMessage("Deal Purchased Failed " + GameManager.Instance.localPlayerID);
			GameManager.Instance.loadingCanvas.LoadingState(_isActive: false, _tip: false);
		});
	}
}
