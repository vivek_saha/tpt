using System;
using UnityEngine;

[Serializable]
public class DealsDataResponse
{
	public string _id;

	public string package_id;

	public string package_name;

	public string chips;

	public string chips_ios;

	public string productid_ios;

	public string productid_android;

	public string discount;

	public string discount_ios;

	public string promoType;

	public string forcesideshow;

	public static DealsDataResponse CreateFromJSON(string data)
	{
		return JsonUtility.FromJson<DealsDataResponse>(data);
	}
}
