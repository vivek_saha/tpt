using System;
using UnityEngine;

[Serializable]
public class TableResponseData
{
	public string status;

	public string responseMessage;

	public string popupMessageTitle;

	public string popupMessageBody;

	public TableResponseDataResponse[] response;

	public static TableResponseData CreateFromJSON(string data)
	{
		return JsonUtility.FromJson<TableResponseData>(data);
	}
}
