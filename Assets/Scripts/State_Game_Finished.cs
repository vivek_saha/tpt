using UnityEngine;

public class State_Game_Finished : MonoBehaviour
{
	[HideInInspector]
	public PlayerElements playerElements;

	private void Awake()
	{
		playerElements = GetComponent<PlayerElements>();
	}

	public void GameFinished()
	{
		playerElements.ResetPlayer();
		playerElements.ResetColor();
		playerElements.tElements.ResetTableElements();
		GameManager.Instance.ResetGameManagerAfterEveryGame();
		playerElements.cardsGO.SetActive(value: false);
	}
}
