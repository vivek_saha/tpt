using System;
using UnityEngine;

public class NumberFormat : MonoBehaviour
{
	public string FormatNumber(long num)
	{
		num = MaxThreeSignificantDigits(num);
		if (num > 0)
		{
			if (num >= 10000000000000L)
			{
				return ((double)num / 1000000000000.0).ToString("0.00L Cr");
			}
			if (num >= 1000000000000L)
			{
				return ((double)num / 1000000000000.0).ToString("0.00L Cr");
			}
			if (num >= 10000000000L)
			{
				return ((double)num / 10000000.0).ToString("#,#Cr");
			}
			if (num >= 1000000000)
			{
				return ((double)num / 10000000.0).ToString("0.0Cr");
			}
			if (num >= 100000000)
			{
				return ((double)num / 10000000.0).ToString("0.0Cr");
			}
			if (num >= 10000000)
			{
				return ((double)num / 10000000.0).ToString("0.00Cr");
			}
			if (num >= 1000000)
			{
				return ((double)num / 100000.0).ToString("0.0L");
			}
			if (num >= 100000)
			{
				return ((double)num / 100000.0).ToString("0.00L");
			}
			if (num >= 10000)
			{
				return ((double)num / 1000.0).ToString("0.0K");
			}
			if (num >= 1000)
			{
				return ((double)num / 1000.0).ToString("0.0K");
			}
			return num.ToString("#,#");
		}
		return "0";
	}

	public string FormatNumberOnTable(long num)
	{
		if (num > 0)
		{
			if (num >= 10000000000000L)
			{
				return ((double)num / 1000000000000.0).ToString("0.00L Cr");
			}
			if (num >= 1000000000000L)
			{
				return ((double)num / 1000000000000.0).ToString("0.00L Cr");
			}
			if (num >= 100000000000L)
			{
				return ((double)num / 10000000.0).ToString("#,#Cr");
			}
			if (num >= 10000000000L)
			{
				return ((double)num / 10000000.0).ToString("#,#Cr");
			}
			if (num >= 1000000000)
			{
				return ((double)num / 10000000.0).ToString("#,#Cr");
			}
			if (num < 1000000000)
			{
				switch (num.ToString().Length)
				{
				case 4:
					return num.ToString().Insert(1, ",");
				case 5:
					return num.ToString().Insert(2, ",");
				case 6:
					return num.ToString().Insert(1, ",").Insert(4, ",");
				case 7:
					return num.ToString().Insert(2, ",").Insert(5, ",");
				case 8:
					return num.ToString().Insert(1, ",").Insert(4, ",")
						.Insert(7, ",");
				case 9:
					return num.ToString().Insert(2, ",").Insert(5, ",")
						.Insert(8, ",");
				}
			}
			return num.ToString("#,#");
		}
		return "0";
	}

	public string FormatNumberOnTableInfo(long num)
	{
		if (num > 0)
		{
			if (num >= 10000000000000L)
			{
				return ((double)num / 1000000000000.0).ToString("0.00L Cr");
			}
			if (num >= 1000000000000L)
			{
				return ((double)num / 1000000000000.0).ToString("0.00L Cr");
			}
			if (num >= 10000000000L)
			{
				return ((double)num / 10000000.0).ToString("#,#Cr");
			}
			if (num >= 1000000000)
			{
				return ((double)num / 10000000.0).ToString("0.0Cr");
			}
			if (num >= 100000000)
			{
				return ((double)num / 10000000.0).ToString("0.0Cr");
			}
			if (num >= 10000000)
			{
				return ((double)num / 10000000.0).ToString("0.0Cr");
			}
			if (num < 10000000)
			{
				switch (num.ToString().Length)
				{
				case 4:
					return num.ToString().Insert(1, ",");
				case 5:
					return num.ToString().Insert(2, ",");
				case 6:
					return num.ToString().Insert(1, ",").Insert(4, ",");
				case 7:
					return num.ToString().Insert(2, ",").Insert(5, ",");
				case 8:
					return num.ToString().Insert(1, ",").Insert(4, ",")
						.Insert(7, ",");
				case 9:
					return num.ToString().Insert(2, ",").Insert(5, ",")
						.Insert(8, ",");
				}
			}
			return num.ToString("#,#");
		}
		return "0";
	}

	internal static long MaxThreeSignificantDigits(long x)
	{
		int num = (int)Math.Log10(x);
		num = Math.Max(0, num - 2);
		num = (int)Math.Pow(10.0, num);
		return x / num * num;
	}
}
