using System;
using UnityEngine;

[Serializable]
public class DownloaderClass
{
	public string imageID;

	public string imageName;

	public int imageIndex;

	public Sprite imgSprite;
}
