using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class Localisation : MonoBehaviour
{
	[HideInInspector]
	public TextMeshProUGUI _text;

	[HideInInspector]
	public Text _defaulttext;

	public string aText;

	public bool useDefaultText;

	private void Awake()
	{
		if (useDefaultText)
		{
			_defaulttext = GetComponent<Text>();
		}
		else
		{
			_text = GetComponent<TextMeshProUGUI>();
		}
	}

	private void Start()
	{
		if (PlayerPrefs.GetInt("Language") == 1)
		{
			if (useDefaultText)
			{
				_defaulttext.font = GameManager.Instance.defaulthindiFont;
				_defaulttext.text = aText;
			}
			else
			{
				_text.font = GameManager.Instance.hindiFont;
				_text.text = aText;
			}
		}
	}
}
