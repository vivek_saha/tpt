using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using UnityEngine;

public class EncDec : MonoBehaviour
{
	public static readonly byte[] ENCKEY = Encoding.ASCII.GetBytes("egvdiDJKSngREiuWGnjkfJKkyDfntreW");

	public static readonly byte[] ENCIV = Encoding.ASCII.GetBytes("vbdRdj5vcsfvcf2d");

	public string Encrypt(string plainText)
	{
		return AESEncrypt(plainText, ENCKEY, ENCIV);
	}

	private string AESEncrypt(string plainText, byte[] Key, byte[] IV)
	{
		if (plainText == null || plainText.Length <= 0)
		{
			throw new ArgumentNullException("plainText");
		}
		if (Key == null || Key.Length <= 0)
		{
			throw new ArgumentNullException("Key");
		}
		if (IV == null || IV.Length <= 0)
		{
			throw new ArgumentNullException("Key");
		}
		byte[] inArray;
		using (AesManaged aesManaged = new AesManaged())
		{
			aesManaged.Key = Key;
			aesManaged.IV = IV;
			ICryptoTransform transform = aesManaged.CreateEncryptor(aesManaged.Key, aesManaged.IV);
			using (MemoryStream memoryStream = new MemoryStream())
			{
				using (CryptoStream stream = new CryptoStream(memoryStream, transform, CryptoStreamMode.Write))
				{
					using (StreamWriter streamWriter = new StreamWriter(stream))
					{
						streamWriter.Write(plainText);
					}
					inArray = memoryStream.ToArray();
				}
			}
		}
		return Convert.ToBase64String(inArray);
	}

	public string Decrypt(string cipherText)
	{
		return AESDecrypt(cipherText, ENCKEY, ENCIV);
	}

	private string AESDecrypt(string cipherText, byte[] Key, byte[] IV)
	{
		if (cipherText == null || cipherText.Length <= 0)
		{
			throw new ArgumentNullException("cipherText");
		}
		byte[] buffer = Convert.FromBase64String(cipherText);
		if (Key == null || Key.Length <= 0)
		{
			throw new ArgumentNullException("Key");
		}
		if (IV == null || IV.Length <= 0)
		{
			throw new ArgumentNullException("Key");
		}
		string text = null;
		using (AesManaged aesManaged = new AesManaged())
		{
			aesManaged.Key = Key;
			aesManaged.IV = IV;
			ICryptoTransform transform = aesManaged.CreateDecryptor(aesManaged.Key, aesManaged.IV);
			using (MemoryStream stream = new MemoryStream(buffer))
			{
				using (CryptoStream stream2 = new CryptoStream(stream, transform, CryptoStreamMode.Read))
				{
					using (StreamReader streamReader = new StreamReader(stream2))
					{
						return streamReader.ReadToEnd();
					}
				}
			}
		}
	}
}
