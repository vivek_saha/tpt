using EasyMobile;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class Settings : MonoBehaviour
{
	public Text settingsNameText;

	public TextMeshProUGUI settingsIDText;

	public Image settingsProfileImage;

	public Sprite defaultSprite;

	public void OpenAboutUsPage()
	{
		Application.OpenURL("http://teenpattitycoon.com/about-us.html");
	}

	public void OpenRateUsPage()
	{
		Application.OpenURL("https://play.google.com/store/apps/details?id=" + Application.identifier);
	}

	/*private void RatingCallback(StoreReview.UserAction action)
	{
		switch (action)
		{
		case StoreReview.UserAction.Refuse:
			break;
		case StoreReview.UserAction.Postpone:
			break;
		case StoreReview.UserAction.Feedback:
			break;
		case StoreReview.UserAction.Rate:
			Application.OpenURL("https://play.google.com/store/apps/details?id=" + Application.identifier);
			break;
		}
	}*/

	public void OpenTermsPage()
	{
		Application.OpenURL("http://teenpattitycoon.com/terms.html");
	}

	public void OpenFBLikeUsPage()
	{
		Application.OpenURL("https://www.facebook.com/teenpattitycoon/");
	}
}
