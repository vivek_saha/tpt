using System;

[Serializable]
public class PlayerLoginJSON
{
	public string tbid;

	public string playerId;

	public string type;

	public string tblBootedAmount;

	public PlayerLoginJSON(string _tbid, string _playerId, string _type, string _tblBootedAmount)
	{
		tbid = _tbid;
		playerId = _playerId;
		type = _type;
		tblBootedAmount = _tblBootedAmount;
	}
}
