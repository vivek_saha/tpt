using System.Collections;
using System.IO;
using TMPro;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class ImageDownloader : MonoBehaviour
{
	private Image img;

	public string image_key;

	public bool loadImageRealTime;

	private bool isDownloading;

	public TextMeshProUGUI textProgress;

	private UnityWebRequest wr;

	private WWW www;

	private void Awake()
	{
		img = GetComponent<Image>();
		img.enabled = false;
	}

	private void Start()
	{
		if (image_key != string.Empty)
		{
			DisplayImageFromDevice(image_key);
		}
		else
		{
			UnityEngine.Debug.LogError("Image Key can't be null");
		}
	}

	private void DisplayImageFromDevice(string _name)
	{
		if (File.Exists(ImageName(_name)))
		{
			byte[] data = File.ReadAllBytes(ImageName(_name));
			Texture2D texture2D = new Texture2D(8, 8);
			texture2D.LoadImage(data);
			Sprite sprite = Sprite.Create(texture2D, new Rect(0f, 0f, texture2D.width, texture2D.height), Vector2.zero);
			img.sprite = sprite;
			img.enabled = true;
		}
		else
		{
			UnityEngine.Debug.LogError("Download Image " + ImageName(_name));
			if (loadImageRealTime && !isDownloading)
			{
				isDownloading = true;
				StartCoroutine(DownloadImageStart(Path.Combine(URL.Instance.ASSET_BUNDLE_URL, image_key + ".png"), image_key));
			}
		}
	}

	private void Update()
	{
		if (!(textProgress == null))
		{
			if (isDownloading)
			{
				textProgress.text = "Downloading ( " + (int)(wr.downloadProgress * 100f) + " % )";
			}
			else
			{
				textProgress.text = string.Empty;
			}
		}
	}

	private string ImageName(string _name)
	{
		return Path.Combine(Application.persistentDataPath, _name) + ".png";
	}

	private IEnumerator DownloadImageStart(string _url, string _name)
	{
		wr = new UnityWebRequest(_url);
		DownloadHandlerTexture texDl = new DownloadHandlerTexture(readable: true);
		wr.downloadHandler = texDl;
		URL.Instance.DebugPrint("IMG DWN : " + _url);
		yield return wr.SendWebRequest();
		if (!wr.isNetworkError && !wr.isHttpError)
		{
			Sprite sprite = Sprite.Create(texDl.texture, new Rect(0f, 0f, texDl.texture.width, texDl.texture.height), Vector2.zero);
			img.sprite = sprite;
			img.enabled = true;
			File.WriteAllBytes(ImageName(_name), wr.downloadHandler.data);
			isDownloading = false;
			URL.Instance.DebugPrint("Image downloading : " + _name + " : " + ImageName(_name));
			PlayerPrefs.SetString(_name, _url);
		}
		else
		{
			URL.Instance.DebugPrint("Image Error DWN : " + wr.error);
		}
	}
}
