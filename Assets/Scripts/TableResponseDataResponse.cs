using System;
using UnityEngine;

[Serializable]
public class TableResponseDataResponse
{
	public string playerId;

	public string chips;

	public string sittingTable;

	public string isActive;

	public string hasCards;

	public string position;

	public string turn;

	public string dealer;

	public string isBlind;

	public CardsArray[] cards;

	public string amount;

	public string timer;

	public string maxTimer;

	public string totalAmount;

	public string msg;

	public string blind;

	public string chaalAmountLimit;

	public string bootedAmountChips;

	public string tableAmountLimit;

	public string type;

	public string forcesideshow;

	public string rewardForceSideShow;

	public string join_number;

	public string variation;

	public string user_id;

	public string facebook;

	public string name;

	public string tempname;

	public string timediff;

	public string lastbonus;

	public string reward;

	public string private_table_code;

	public string messages;

	public string var_type;

	public string preAmount;

	public string firstPerson;

	public string secondPerson;

	public string itemNumber;

	public string sideShow;

	public string sstimer;

	public string ssmaxTimer;

	public string force;

	public string maxBlind;

	public string profile;

	public string forceDeduct;

	public string newVip;

	public int MMmode;

	public static TableResponseDataResponse CreateFromJSON(string data)
	{
		return JsonUtility.FromJson<TableResponseDataResponse>(data);
	}
}
