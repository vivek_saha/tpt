using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class ShakeIt : MonoBehaviour
{
	public Image img;

	private void Start()
	{
		InvokeRepeating("Fill", 5f, 10f);
	}

	private void Fill()
	{
		img.DOFillAmount(1f, 0.5f).SetEase(Ease.OutBack).OnComplete(delegate
		{
			img.transform.DOShakeRotation(1f, 10f).OnComplete(delegate
			{
				img.DOFillAmount(0f, 0.5f).SetEase(Ease.InBack).SetDelay(3f);
			});
		});
	}
}
