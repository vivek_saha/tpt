using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class LoadLoginElements : MonoBehaviour
{
	public TextMeshProUGUI fbChipsTextOnly;

	public RawImage rawImage;

	private void Start()
	{
		if (GameManager.Instance.fbChips.Length > 0)
		{
			fbChipsTextOnly.text = GameManager.Instance.format.FormatNumberOnTable(long.Parse(GameManager.Instance.fbChips));
		}
		GameManager.Instance.loadingCanvas.LoadingState(_isActive: false, _tip: false);
		if (PlayerPrefs.GetFloat("Facebook") == 1f)
		{
			GameManager.Instance.loadingCanvas.LoadingState(_isActive: true, _tip: false);
			FacebookLogin();
		}
	}

	public void GuestLogin()
	{
		GameManager.Instance.loginManager.GuestLoginCO();
	}

	public void FacebookLogin()
	{
		GameManager.Instance.loginManager.LoginWithFacebook();
	}

	public IEnumerator DownloadPinky()
	{
		RawImage obj = rawImage;
		Color color = rawImage.color;
		float r = color.r;
		Color color2 = rawImage.color;
		float g = color2.g;
		Color color3 = rawImage.color;
		obj.color = new Color(r, g, color3.b, 0f);
		WWW www = new WWW(GameManager.Instance.pinkyURL);
		yield return www;
		rawImage.texture = www.texture;
		RawImage obj2 = rawImage;
		Color color4 = rawImage.color;
		float r2 = color4.r;
		Color color5 = rawImage.color;
		float g2 = color5.g;
		Color color6 = rawImage.color;
		obj2.color = new Color(r2, g2, color6.b, 255f);
	}
}
