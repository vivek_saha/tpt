using UnityEngine;
using UnityEngine.UI;

public class OpenLink : MonoBehaviour
{
	public Button fblogin;

	public Button guestlogin;

	public Toggle termsToggle;

	private void Start()
	{
		termsToggle.isOn = true;
	}

	public void OpenTermsLink()
	{
		Application.OpenURL("http://teenpattitycoon.com/terms.html");
		AudioManager.instance.PlaySound("click");
		GameManager.Instance.LogMessage("Terms Page");
	}

	private void Update()
	{
		if (termsToggle.isOn)
		{
			fblogin.interactable = true;
			guestlogin.interactable = true;
		}
		else
		{
			fblogin.interactable = false;
			guestlogin.interactable = false;
		}
	}
}
