using EasyMobile;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using TMPro;
using UniRx;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LoadLevel : MonoBehaviour
{
	[SerializeField]
	private float timer;

	[SerializeField]
	private Slider loadingSlider;

	[SerializeField]
	private TextMeshProUGUI loadingText;

	[SerializeField]
	private TextMeshProUGUI versionText;

	[SerializeField]
	private bool canIncreaseSlider;

	public OpenClosePopup upgradeGO;

	public OpenClosePopup maintenanceGO;

	public TextMeshProUGUI defaultmaintenanceText;

	public TextMeshProUGUI maintenanceText;

	[Header("Root/Jailbreak")]
	public OpenClosePopup rootjailGO;

	public TextMeshProUGUI rootjailTitleText;

	public TextMeshProUGUI rootjailBodyText;

	[SerializeField]
	private double maintenanceTimeRemaining;

	private bool isGameInMaintenance;

	private bool isImagesDownloaded;

	private bool downloadStarted;

	private bool canReadResponsesFromImageQueue;

	private Queue<string> downloadImagesQueue = new Queue<string>();

	private UnityWebRequest wr;

	private WWW www;

	public int imgCounter;

	public int downloadedImageLength;

	private string[] paths;

	private bool rooted;

	private void Start()
	{
		loadingSlider.value = 0f;
		versionText.text = "V-" + GameManager.Instance.versionNumber;
		GameManager.Instance.LanguageConverter(loadingText, "ne$vkR se kneK$ hae rha heW -", "Connecting to server....");
		GameManager.Instance.loadingCanvas.LoadingState(_isActive: false, _tip: false);
		if (check_jailbroken())
		{
			rooted = true;
			rootjailGO.OpenPopup();
			rootjailTitleText.text = "Root detected!.";
			rootjailBodyText.text = "TPT is not supported on rooted devices. Please restore or update your device in order to run TPT.";
			GameManager.Instance.LogMessage("Root Detected : " + SystemInfo.deviceUniqueIdentifier);
		}
		else
		{
			rooted = false;
			DownloadImagesAPI();
		}
	}

	private void Update()
	{
		if (rooted)
		{
			return;
		}
		if (Application.internetReachability == NetworkReachability.ReachableViaCarrierDataNetwork || Application.internetReachability == NetworkReachability.ReachableViaLocalAreaNetwork)
		{
			if (isImagesDownloaded)
			{
				if (canIncreaseSlider && !isGameInMaintenance)
				{
					if (loadingSlider.value < timer / 5f)
					{
						loadingSlider.value += Time.deltaTime / 5f;
					}
					GameManager.Instance.LanguageConverter(loadingText, " @eek kI sei$'g ", "Setting up the deck!!");
					if (loadingSlider.value >= timer / 5f * 0.3f && loadingSlider.value < timer * 0.6f)
					{
						GameManager.Instance.LanguageConverter(loadingText, "ka@R imlana", "Shuffling the cards..");
					}
					if (loadingSlider.value >= timer / 5f * 0.6f && loadingSlider.value < timer * 0.8f)
					{
						GameManager.Instance.LanguageConverter(loadingText, "i%lai@yo' kI sei$'g ", "Setting up the players..");
					}
					if (loadingSlider.value >= timer / 5f * 0.8f)
					{
						GameManager.Instance.LanguageConverter(loadingText, "sb ho gya - %el ka Aan'd le' -", "All Done!! Enjoy the Game :)");
					}
					if (loadingSlider.value >= timer / 5f)
					{
						GameManager.Instance.LogMessage("Loading Complete");
						if (PlayerPrefs.HasKey("Language"))
						{
							SceneManager.LoadScene(URL.Instance.LOGIN_SCENE);
						}
						else
						{
							SceneManager.LoadScene(URL.Instance.LANGUAGE_SCENE);
						}
					}
				}
				else
				{
					loadingSlider.value = 0f;
					if (isGameInMaintenance)
					{
						if (maintenanceTimeRemaining > 0.0)
						{
							maintenanceTimeRemaining -= Time.deltaTime;
							loadingText.text = string.Empty;
						}
						else
						{
							maintenanceTimeRemaining = 0.0;
						}
						TimeSpan timeSpan = TimeSpan.FromSeconds(maintenanceTimeRemaining);
						string str = $"{timeSpan.Hours:D2}h : {timeSpan.Minutes:D2}m : {timeSpan.Seconds:D2}s";
						GameManager.Instance.LanguageConverter(defaultmaintenanceText, "saerI ne$vkR meeeeN$eneNs me hW - plIj pUn kaeiXaXa kree - ", "Sorry, the servers are under maintenance.Please try again in about");
						maintenanceText.text = "\n\n<size=130><#FFFF00FF>" + str + "</color></size>";
					}
				}
			}
			if (downloadStarted)
			{
				loadingSlider.value = wr.downloadProgress;
			}
			if (canReadResponsesFromImageQueue && downloadImagesQueue.Count > 0)
			{
				canReadResponsesFromImageQueue = false;
				DownloadAndCheckImages(downloadImagesQueue.Peek());
			}
		}
		else
		{
			loadingSlider.value = 0f;
			loadingText.text = "Please connect to internet!";
		}
	}

	public bool CheckResponse(string _data, string[] _fields)
	{
		JSONObject jSONObject = JSONObject.Create(_data);
		if (_fields.Length == 1)
		{
			return jSONObject.HasField(_fields[0]);
		}
		if (jSONObject.HasField(_fields[0]))
		{
			JSONObject field = jSONObject.GetField(_fields[0]);
			return CheckResponse(field.ToString(), _fields.Where((string val, int idx) => idx != 0).ToArray());
		}
		UnityEngine.Debug.LogWarning(_fields[0] + " field not exist");
		return false;
	}

	public void VersionCheck()
	{
		ObservableWWW.Get(URL.Instance.CALLAPI(URL.Instance.VERSION_CHECK, string.Empty)).Subscribe(delegate(string x)
		{
			URL.Instance.DebugPrint("VERSION_CHECK " + GameManager.Instance.security.Decrypt(x));
			PlayerData playerData = PlayerData.CreateFromJSON(GameManager.Instance.security.Decrypt(x));
			Debug.LogWarning(CheckResponse(GameManager.Instance.security.Decrypt(x), new string[2]
			{
				"response",
				"fbChips"
			}));
			if (playerData.status == "success")
			{
				if (GameManager.Instance.versionNumber == playerData.response.version)
				{
					GameManager.Instance.fbChips = playerData.response.fbChips;
					GameManager.Instance.pinkyURL = playerData.response.pinkyURL;
					MaintenanceMode();
				}
				else
				{
					URL.Instance.DebugPrint("Please Update the version.");
					NativeUI.ShowToast("Please Update the version.", isLongToast: true);
					loadingText.text = "Please update the game.";
					upgradeGO.OpenPopup();
				}
			}
		}, delegate(Exception ex)
		{
			Debug.LogException(ex);
			Invoke("VersionCheck", 2f);
			NativeUI.ShowToast("Oh Snap! Something went wrong :(", isLongToast: true);
		});
	}

	public void MaintenanceMode()
	{
		ObservableWWW.Get(URL.Instance.CALLAPI(URL.Instance.MAINTENANCE_CHECK, string.Empty)).Subscribe(delegate(string x)
		{
			URL.Instance.DebugPrint("MAINTENANCE_CHECK " + GameManager.Instance.security.Decrypt(x));
			PlayerData playerData = PlayerData.CreateFromJSON(GameManager.Instance.security.Decrypt(x));
			if (playerData.status == "success")
			{
				isGameInMaintenance = true;
				maintenanceTimeRemaining = double.Parse(playerData.response.seconds);
				maintenanceGO.OpenPopup();
				NativeUI.ShowToast("Maintenance is on.", isLongToast: true);
			}
			else
			{
				isGameInMaintenance = false;
				canIncreaseSlider = true;
			}
		}, delegate(Exception ex)
		{
			UnityEngine.Debug.LogException(ex);
			Invoke("MaintenanceMode", 2f);
			NativeUI.ShowToast("Oh Snap! Something went wrong :(", isLongToast: true);
		});
	}

	private bool check_jailbroken()
	{
		paths = new string[10]
		{
			"/system/app/Superuser.apk",
			"/sbin/su",
			"/system/bin/su",
			"/system/xbin/su",
			"/data/local/xbin/su",
			"/data/local/bin/su",
			"/system/sd/xbin/su",
			"/system/bin/failsafe/su",
			"/data/local/su",
			"/su/bin/su"
		};
		bool result = false;
		for (int i = 0; i < paths.Length; i++)
		{
			if (File.Exists(paths[i]))
			{
				result = true;
			}
		}
		return result;
	}

	private void DownloadImagesAPI()
	{
		URL.Instance.DebugPrint("DOWNLOAD_IMAGES : " + URL.Instance.CALLAPI(URL.Instance.DOWNLOAD_IMAGES));
		ObservableWWW.Get(URL.Instance.CALLAPI(URL.Instance.DOWNLOAD_IMAGES)).Subscribe(delegate(string x)
		{
			URL.Instance.DebugPrint("DOWNLOAD_IMAGES : " + GameManager.Instance.security.Decrypt(x));
			GameManager.Instance.LanguageConverter(loadingText, "%el s'sa/no kI ja'c", "Checking game resources...");
			DownloadImageData downloadImageData = DownloadImageData.CreateFromJSON(GameManager.Instance.security.Decrypt(x));
			if (downloadImageData.status == "success")
			{
				downloadedImageLength = downloadImageData.response.Length;
				for (int i = 0; i < downloadImageData.response.Length; i++)
				{
					URL.Instance.DebugPrint(downloadImageData.response[i].image_key);
					downloadImagesQueue.Enqueue(downloadImageData.response[i].image_key + "~" + downloadImageData.response[i].image_name + "~" + downloadImageData.response[i].image_url);
				}
				canReadResponsesFromImageQueue = true;
			}
		}, delegate(Exception ex)
		{
			Debug.LogException(ex);
			NativeUI.ShowToast("Oh Snap! Something went wrong :(", isLongToast: true);
		});
	}

	private void DownloadAndCheckImages(string _str)
	{
		downloadImagesQueue.Dequeue();
		char[] separator = new char[2]
		{
			' ',
			'~'
		};
		string[] array = _str.Split(separator);
		StopCoroutine("DownloadImagesCO");
		DownloadImagesCO(array[2], array[0]);
	}

	private void DownloadImagesCO(string _url, string _name)
	{
		imgCounter++;
		if (File.Exists(ImageName(_name)))
		{
			byte[] data = File.ReadAllBytes(ImageName(_name));
			Texture2D texture2D = new Texture2D(8, 8);
			texture2D.LoadImage(data);
			Sprite sprite = Sprite.Create(texture2D, new Rect(0f, 0f, texture2D.width, texture2D.height), Vector2.zero);
			URL.Instance.DebugPrint("Image already downloaded : " + _name);
			if (PlayerPrefs.HasKey(_name) && PlayerPrefs.GetString(_name) != _url)
			{
				downloadStarted = true;
				StartCoroutine(DownloadImageStart(_url, _name));
			}
			InitVersionCheck();
		}
		else
		{
			downloadStarted = true;
			StartCoroutine(DownloadImageStart(_url, _name));
		}
	}

	private IEnumerator DownloadImageStart(string _url, string _name)
	{
		wr = new UnityWebRequest(_url);
		DownloadHandlerTexture texDl = new DownloadHandlerTexture(readable: true);
		wr.downloadHandler = texDl;
		URL.Instance.DebugPrint(wr.downloadHandler.data.Length);
		loadingText.text = "Download is in progress (" + imgCounter.ToString() + "/" + downloadedImageLength.ToString() + ")";
		yield return wr.SendWebRequest();
		if (!wr.isNetworkError && !wr.isHttpError)
		{
			Sprite.Create(texDl.texture, new Rect(0f, 0f, texDl.texture.width, texDl.texture.height), Vector2.zero);
			File.WriteAllBytes(ImageName(_name), wr.downloadHandler.data);
			URL.Instance.DebugPrint("Image downloading : " + _name + " : " + ImageName(_name));
			PlayerPrefs.SetString(_name, _url);
			InitVersionCheck();
		}
		else
		{
			URL.Instance.DebugPrint("Image Error : " + wr.error);
		}
	}

	private void InitVersionCheck()
	{
		canReadResponsesFromImageQueue = true;
		if (downloadImagesQueue.Count == 0)
		{
			downloadStarted = false;
			isImagesDownloaded = true;
			VersionCheck();
		}
	}

	private string ImageName(string _name)
	{
		return Path.Combine(Application.persistentDataPath, _name) + ".png";
	}
}
