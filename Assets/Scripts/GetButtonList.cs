using UnityEngine;
using UnityEngine.UI;

public class GetButtonList : MonoBehaviour
{
	public Button[] buttons;

	public void DisableButtons()
	{
		for (int i = 0; i < buttons.Length; i++)
		{
			buttons[i].interactable = false;
		}
	}

	public void EnableButtons()
	{
		for (int i = 0; i < buttons.Length; i++)
		{
			buttons[i].interactable = true;
		}
	}
}
