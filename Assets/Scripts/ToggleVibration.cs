using UnityEngine;
using UnityEngine.UI;

public class ToggleVibration : MonoBehaviour
{
	[SerializeField]
	private Sprite vibrationOn;

	[SerializeField]
	private Sprite vibrationOff;

	[SerializeField]
	private Image vibrationButtonImage;

	private void OnEnable()
	{
		if (PlayerPrefs.GetInt("Vibration") == 0)
		{
			vibrationButtonImage.sprite = vibrationOff;
		}
		else
		{
			vibrationButtonImage.sprite = vibrationOn;
		}
	}

	public void ToggleVibrations()
	{
		Debug.Log("Vibration Testing..." + PlayerPrefs.GetInt("Vibration"));
		if (PlayerPrefs.GetInt("Vibration") == 0)
		{
			PlayerPrefs.SetInt("Vibration", 1);
			vibrationButtonImage.sprite = vibrationOn;
			Handheld.Vibrate();
		}
		else
		{
			PlayerPrefs.SetInt("Vibration", 0);
			vibrationButtonImage.sprite = vibrationOff;
		}
	}
}
